USE [CVApp2]
GO
/****** Object:  StoredProcedure [dbo].[SearchMatchCV]    Script Date: 01/23/2017 09:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[SearchCVNames]
	@keywords nvarchar(MAX) = null,
	@regionIds nvarchar(Max) = null,
	@tagIds nvarchar(Max) = null,
	@skills nvarchar(Max) = null,
	@status bit = null
	 
AS
BEGIN

SET NOCOUNT ON;

	CREATE TABLE #KeywordCandidates
	(
		Id int not null
	)

	Declare
	@returnAll bit =1,
	@searchKeywords bit,
	@originalkeywords nvarchar(4000),
	@sql nvarchar(max),
	@sql_orderby nvarchar(max)
	
	--set @keywords='s';
	if(@keywords is not null)
	begin
		set @keywords=rtrim(ltrim(@keywords))
		set @originalkeywords=@keywords
		set @keywords='%'+@keywords+'%'
		
		set @searchKeywords =1
		set @returnAll=0

    -- using Union as they are much faster than OR condition		
	set @sql= '
	Insert into #KeywordCandidates(Id)
	select c.Id
	from Candidates c with (NOLOCK)
	where PatIndex(@keywords,c.FirstName) >0 

	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.LastName) > 0 
	
	


	'
	

	
	exec sp_executesql @sql, N'@keywords nvarchar(4000)',@keywords

	end

    -- filter by selected regions	
	set @regionIds=isnull(@regionIds,'')
	
	Create Table #FilteredRegionIds
	(
	     RegionId int not null
	)
	
	Insert into #FilteredRegionIds(RegionId)
	Select Cast(data as int) from splitstring_to_table(@regionIds,',')

	Declare @regionIdsCount int
	set @regionIdsCount = (SELECT COUNT(1) FROM #FilteredRegionIds)
	IF(@regionIdsCount>0) set @returnAll=0
	-----

	---- profession tags filter
	Declare @tagIdsCount int
	set @tagIds=isnull(@tagIds,'')

	if @tagIds<>''
	BEGIN
		Create table #FilteredTagIds ( TagId int not null )
		insert into #FilteredTagIds(TagId)
			select cast(data as int) from splitstring_to_table(@tagIds,',')

		set @tagIdsCount=(select count(1) from #FilteredTagIds)
		if(@tagIdsCount>0) set @returnAll=0
	END

	----

	----- Skills filtering
	Declare @skillsCount int
	set @skills=isnull(@skills,'')

	if @skills<>''
	BEGIN
		Create table #FilteredSkills ( Skill nvarchar(250) not null )
		insert into #FilteredSkills(Skill)
			select data from splitstring_to_table(@skills,',')

		set @skillsCount=(select count(1) from #FilteredSkills)
		if(@skillsCount>0) set @returnAll=0
	END


	-----

	--- status filtering
	if @status is not null 
	BEGIN
		set @returnAll=0
	END

	---

	CREATE TABLE #selectedCandidates (
		RecordId int Identity not null,
		CandidateId int not null
	)

	if @returnAll=1
	BEGIN
		SET @sql='
		insert into #selectedCandidates(CandidateId)
		SELECT c.Id 
		FROM Candidates c
		'
	END
	ELSE
	BEGIN
		DECLARE @whereTag varchar(20) = ' where'

		SET @sql='
		
		insert into #selectedCandidates(CandidateId)
		select c.Id
		FROM Candidates c
		Left join ZipCodes z on z.Id=c.ZipcodeId
		Left JOIN Cities ct on z.CityId=ct.Id
		Left join CandidateTags tag on c.Id=tag.CandidateId
		Left join CandidateSkills skill on c.Id=skill.CandidateId
		'
		
		IF(@searchKeywords=1)
			BEGIN
				SET @sql= @sql + ' 
				 INNER JOIN #KeywordCandidates k ON c.Id = k.Id
				'
			END

		IF(@regionIdsCount>0)
			BEGIN
				SET @sql=@sql+ @whereTag + '
				CT.RegionId IN (SELECT RegionId FROM #FilteredRegionIds)
				'
				SET @whereTag = ' AND '
			END

		if(@tagIdsCount>0)
		BEGIN
			set @sql=@sql + @whereTag + ' 
			tag.TagId in (SELECT TagId from #FilteredTagIds)
			'
			SET @whereTag=' AND '
		END

		if(@skillsCount>0)
		BEGIN
			set @sql=@sql + @whereTag + ' 
			skill.Skill in (SELECT Skill from #FilteredSkills)
			'
			SET @whereTag=' AND '
		END

		if @status is not null 
		BEGIN
			set @sql=@sql + @whereTag + ' 
			Status=' + CONVERT(nvarchar(10), @status  )
			SET @whereTag=' AND '
		END

	END

	set @sql=@sql+ 'group by c.Id order by c.id'
	
	exec sp_executesql @sql


	CREATE TABLE #selectedSortedCandidates (
		RecordId int Identity not null,
		CandidateId int not null
	)

	insert into #selectedSortedCandidates(CandidateId)  select s.CandidateId from #selectedCandidates s  
	inner join Candidates c on s.CandidateId = c.Id
	left join Zipcodes z on c.ZipcodeId = z.Id
	order by z.Zipcode

	SELECT top 10 (c.FirstName + ' ' + c.lastname) as Name ,ct.City,z.Zipcode FROM #selectedSortedCandidates	s
	inner join Candidates c on s.CandidateId = c.Id
	left join Zipcodes z on c.ZipcodeId = z.Id
	left join Cities ct on z.CityId=ct.Id
	order by 1



DROP TABLE #KeywordCandidates
DROP TABLE #selectedCandidates
		
	RETURN 0
END
