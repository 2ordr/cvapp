USE [CVApp2]
GO
/****** Object:  StoredProcedure [dbo].[SearchCV]    Script Date: 09-06-2017 09:08:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[SearchCV]
	@keywords nvarchar(MAX) = null,
	@regionIds nvarchar(Max) = null,
	@tagIds nvarchar(Max) = null,
	@skills nvarchar(Max) = null,
	@status nvarchar(50) = null,
	@fromDate nvarchar(100) =null,
	@toDate nvarchar(100) = null,
	@dateType int =0,
	@pageIndex int =1,
	@pageSize int = 100,
	@pageCount int output,
	@recordCount int output
AS
BEGIN

SET NOCOUNT ON;

	CREATE TABLE #KeywordCandidates
	(
		Id int not null
	)

	Declare
	@returnAll bit =1,
	@searchKeywords bit,
	@originalkeywords nvarchar(4000),
	@sql nvarchar(max),
	@sql_orderby nvarchar(max)

	if(@keywords is not null)
	begin
		set @keywords=ltrim(@keywords)
		set @originalkeywords=@keywords
		set @keywords='%'+@keywords+'%'
		
		set @searchKeywords =1
		set @returnAll=0

    -- using Union as they are much faster than OR condition		
	set @sql= '
	Insert into #KeywordCandidates(Id)
	select c.Id
	from Candidates c with (NOLOCK)
	where PatIndex(@keywords,c.FirstName) >0 or
	      (FirstName<>'''' and  ( dbo.Metaphone(FirstName)=dbo.Metaphone(@keywords) and dbo.Metaphone(@keywords) <> ''error with''  )  )

	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.LastName) > 0  or
	      (LastName<>'''' and  ( dbo.Metaphone(LastName)=dbo.Metaphone(@keywords) and dbo.Metaphone(@keywords) <> ''error with''  )  )

	Union
    select c.id 
    from Candidates c with (NOLOCK)
    where PatIndex(@Keywords,c.FirstName + '' '' + c.LastName) > 0 or
	      (LastName<>'''' and  ( dbo.Metaphone(FirstName + '' '' + c.LastName)=dbo.Metaphone(@keywords) and dbo.Metaphone(@keywords) <> ''error with''  )  )
	
	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.RegistrationId) > 0 


	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.Designation) > 0 

	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.PhoneHome) > 0 
	
	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.PhoneWork) > 0 

	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.PhoneMobile) > 0 

	
	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.Address) > 0 

	Union
	select c.id 
	from Candidates c with (NOLOCK)
	where PatIndex(@Keywords,c.Notes) > 0 

	union 
	Select c.id
	from Candidates c with (NOLOCK)
	inner join Zipcodes z on c.ZipcodeId=z.Id
	where PatIndex(@Keywords,z.Zipcode) > 0 
	
	Union
	select s.CandidateId
	from CandidateSkills s with (NOLOCK)
	where PatIndex(@keywords,s.Skill) >0

	Union
	select t.CandidateId
	from CandidateTags t with (NOLOCK) 
	inner join ProfessionTags p on t.TagId = p.Id
	where PatIndex(@Keywords,p.Tag) >0
	
	Union
	select fl.CandidateId 
	from CandidateFiles fl with (NOLOCK)
	where PatIndex(@Keywords,fl.FileContent) > 0


	'
	

	
	exec sp_executesql @sql, N'@keywords nvarchar(4000)',@keywords

	end

    -- filter by selected regions	
	set @regionIds=isnull(@regionIds,'')
	
	Create Table #FilteredRegionIds
	(
	     RegionId int not null
	)
	
	Insert into #FilteredRegionIds(RegionId)
	Select Cast(data as int) from splitstring_to_table(@regionIds,',')

	Declare @regionIdsCount int
	set @regionIdsCount = (SELECT COUNT(1) FROM #FilteredRegionIds)
	IF(@regionIdsCount>0) set @returnAll=0
	-----

	---- profession tags filter
	Declare @tagIdsCount int
	set @tagIds=isnull(@tagIds,'')

	if @tagIds<>''
	BEGIN
		Create table #FilteredTagIds ( TagId int not null )
		insert into #FilteredTagIds(TagId)
			select cast(data as int) from splitstring_to_table(@tagIds,',')

		set @tagIdsCount=(select count(1) from #FilteredTagIds)
		if(@tagIdsCount>0) set @returnAll=0
	END

	----

	----- Skills filtering
	Declare @skillsCount int
	set @skills=isnull(@skills,'')

	if @skills<>''
	BEGIN
		Create table #FilteredSkills ( Skill nvarchar(250) not null )
		insert into #FilteredSkills(Skill)
			select data from splitstring_to_table(@skills,',')

		set @skillsCount=(select count(1) from #FilteredSkills)
		if(@skillsCount>0) set @returnAll=0
	END


	-----

	--- status filtering
	if @status is not null 
	BEGIN
		set @returnAll=0
	END

	---

	if @fromDate is not null
	BEGIN
	  set @returnAll=0
	END


    if @toDate is not null
	BEGIN
	  set @returnAll=0
	END


	CREATE TABLE #selectedCandidates (
		RecordId int Identity not null,
		CandidateId int not null
	)

	if @returnAll=1
	BEGIN
		SET @sql='
		insert into #selectedCandidates(CandidateId)
		SELECT c.Id 
		FROM Candidates c
		'
	END
	ELSE
	BEGIN
		DECLARE @whereTag varchar(20) = ' where'

		SET @sql='
		
		insert into #selectedCandidates(CandidateId)
		select c.Id
		FROM Candidates c
		Left join ZipCodes z on z.Id=c.ZipcodeId
		Left JOIN Cities ct on z.CityId=ct.Id
		Left join CandidateTags tag on c.Id=tag.CandidateId
		Left join CandidateSkills skill on c.Id=skill.CandidateId
		'
		
		IF(@searchKeywords=1)
			BEGIN
				SET @sql= @sql + ' 
				 INNER JOIN #KeywordCandidates k ON c.Id = k.Id
				'
			END

		IF(@regionIdsCount>0)
			BEGIN
				SET @sql=@sql+ @whereTag + '
				CT.RegionId IN (SELECT RegionId FROM #FilteredRegionIds)
				'
				SET @whereTag = ' AND '
			END

		if(@tagIdsCount>0)
		BEGIN
			IF(@tagIds='-1')
		    BEGIN
				set @sql=@sql + @whereTag + ' tag.Id is null '
			END
			ELSE
			BEGIN
				set @sql=@sql + @whereTag + ' 
				tag.TagId in (SELECT TagId from #FilteredTagIds)
				'
			END

			SET @whereTag=' AND '
		END

		if(@skillsCount>0)
		BEGIN
			set @sql=@sql + @whereTag + ' 
			skill.Skill in (SELECT Skill from #FilteredSkills)
			'
			SET @whereTag=' AND '
		END

		if NULLIF(@status,'') is not null 
		BEGIN
			set @sql=@sql + @whereTag + ' 
			Status in (' + @status  + ')'
			SET @whereTag=' AND '
		END


		declare @dateField nvarchar(50)
		set @dateField = case @dateType
		                 WHEN 0 THEN 'Created'
						 WHEN 1 THEN 'LastUpdated'
						 ELSE	'LastAccessed'
						 END

		if NULLIF(@fromDate,'') is not null 
		BEGIN
			set @sql=@sql + @whereTag + ' ' + @dateField +
			' >=''' + @fromDate + ''''
			SET @whereTag=' AND '
		END


		if NULLIF(@toDate,'') is not null 
		BEGIN
			set @sql=@sql + @whereTag + ' ' + @dateField + 
			' <='''  + @toDate + ''''
			SET @whereTag=' AND '
		END



	END

	set @sql=@sql+ 'group by c.Id order by c.id'
	print @sql

	exec sp_executesql @sql

	select @recordCount = count(*) from #selectedCandidates
	set @pageCount=CEILING(CAST(@RecordCount AS DECIMAL(10, 2)) / CAST(@PageSize AS DECIMAL(10, 2)))

	select * into #skillTemp from CandidateSkills where Id in (select Id from #selectedCandidates)

	select
    CandidateId, 
	    stuff((
        select ',' + t.[Skill]
        from #skillTemp t
        where t.CandidateId = t1.CandidateId
        order by t.[Skill]
        FOR XML PATH, TYPE).value('.[1]',
		 'nvarchar(max)'), 1, 1, '') as skillsCSV
	into #skillCSV
	from #skillTemp t1
	group by CandidateId

	select CandidateId,FileName into #filesTemp from CandidateFiles

	select
    CandidateId, 
	    stuff((
        select '|' + t.[FileName]
        from #filesTemp t
        where t.CandidateId = t1.CandidateId
        order by t.[FileName]
        FOR XML PATH, TYPE).value('.[1]',
		 'nvarchar(max)'), 1, 1, '') as filesCSV
	into #filesCSV
	from #filesTemp t1
	group by CandidateId


	CREATE TABLE #selectedSortedCandidates (
		RecordId int Identity not null,
		CandidateId int not null
	)

	insert into #selectedSortedCandidates(CandidateId)  select s.CandidateId from #selectedCandidates s  
	inner join Candidates c on s.CandidateId = c.Id
	left join Zipcodes z on c.ZipcodeId = z.Id
	order by z.Zipcode

	SELECT c.*,csv.skillsCSV,ct.City,z.Zipcode,fl.filesCSV FROM #selectedSortedCandidates	s
	inner join Candidates c on s.CandidateId = c.Id
	left join #skillCSV csv on s.CandidateId=csv.CandidateId
	left join Zipcodes z on c.ZipcodeId = z.Id
	left join Cities ct on z.CityId=ct.Id
	left join #filesCSV fl on fl.CandidateId=s.CandidateId
	where RecordId between(@pageIndex-1) * @pageSize +1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1  order by z.Zipcode



DROP TABLE #KeywordCandidates
--DROP TABLE #FilteredRegionIds
--DROP TABLE #FilteredTagIds
--DROP TABLE #FilteredSkills
DROP TABLE #selectedCandidates
		
	RETURN 0
END
