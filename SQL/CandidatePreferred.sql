USE [CVApp]
GO

/****** Object:  Table [dbo].[CandidatePreferredRegions]    Script Date: 11/1/2016 8:34:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CandidatePreferredRegions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CandidateId] [int] NOT NULL,
	[RegionId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CandidatePreferredRegions]  WITH CHECK ADD  CONSTRAINT [FK_CandidatePreferredRegions_ToCandidates] FOREIGN KEY([CandidateId])
REFERENCES [dbo].[Candidates] ([Id])
GO

ALTER TABLE [dbo].[CandidatePreferredRegions] CHECK CONSTRAINT [FK_CandidatePreferredRegions_ToCandidates]
GO

ALTER TABLE [dbo].[CandidatePreferredRegions]  WITH CHECK ADD  CONSTRAINT [FK_CandidatePreferredRegions_ToRegions] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Regions] ([Id])
GO

ALTER TABLE [dbo].[CandidatePreferredRegions] CHECK CONSTRAINT [FK_CandidatePreferredRegions_ToRegions]
GO

