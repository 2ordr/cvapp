﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class CandidateSkillService : ICandidateSkillService
    {
        public void AddNew(Models.CandidateSkill entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "CandidateSkills", string.Format(" candidateId={0} and skill='{1}'", entity.CandidateId, entity.Skill));

            if (exists != null)
                throw new InvalidOperationException("Already exists");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into CandidateSkills(CandidateId,Skill) values({0},'{1}')",
                    entity.CandidateId,
                    entity.Skill))
                .MyExecuteNonQuery();


            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

        }

        public void Update(Models.CandidateSkill entity)
        {
            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Update CandidateSkills set CandidateId={0}, Skill='{1}' where Id={2}",
                entity.CandidateId,
                entity.Skill,
                entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();
        }

        public void Delete(Models.CandidateSkill entity)
        {
            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidateSkills where Id={0}",
                entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();
        }

        public List<CandidateSkill> GetCandidateSkills(int candidateId)
        {
            List<CandidateSkill> result = new List<CandidateSkill>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidateSkills where candidateId={0}", candidateId))
                .MyExecuteReader(record =>
                {
                    result.Add(new CandidateSkill
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        Skill = record.SafeRead<string>("Skill")
                    });
                });

            return result;
        }

        public CandidateSkill GetCandidateSkillsByID(int id)
        {
            CandidateSkill candidateSkill = new CandidateSkill();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from CandidateSkills where Id=" + id)
                .MyExecuteReader(record =>
                {
                    candidateSkill.Id = id;
                    candidateSkill.CandidateId = record.SafeRead<int>("CandidateId");
                    candidateSkill.Skill = record.SafeRead<string>("Skill");
                });

            return candidateSkill;
        }
    }
}