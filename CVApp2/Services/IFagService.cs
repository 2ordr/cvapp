﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IFagService 
    {
        void AddNew(Fag Fag);
        void Update(Fag Fag);

        void Delete(Fag Fag);

        List<Fag> GetAll();

        Fag GetFagByID(int id);

        List<Fag> SearchFag(string keywords);
    }
}