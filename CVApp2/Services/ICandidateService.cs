﻿using CVApp2.Models;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public enum DateType
    {
        Created=0,
        LastUpdated=1,
        LastAccessed=2
    }

    public interface ICandidateService : IServiceBase<Candidate>
    {

        List<CandidateListDetails> SearchPaged(string keyword, string regionIds, string tagIds, string skills, string status, string fromDate, string toDate, DateType dateType, int pageIndex, int pageSize, out int pageCount, out int recordCount);

        List<Candidate> GetCandidates(DateType dateType, Tuple<DateTime, DateTime> dateRange);

        List<string> AllSkills();

        Candidate GetCandidate(int id);

        //CandidateDetails GetCandidateDetails(int id);

        Candidate GetCandidateDetails(int id);


        List<string> GetMatchedList(string keyword, string regionIds, string tagIds, string skills, bool? status);


        List<Object> CustomersFromSharepoint(string registrationNumber);

        Candidate CustomerFromSharepoint(string registrationNumber);

        void UpdateDates(DateType type, int candidateId);
    }
}