﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IDocumentGroupService : IServiceBase<DocumentGroup>
    {
        List<DocumentGroup> GetAll();

        DocumentGroup GetDocumentGroupById(int id);
    }
}