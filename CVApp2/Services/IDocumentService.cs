﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IDocumentService : IServiceBase<Document>
    {
        List<Document> GetAll();

        List<Document> GetFiltered(string groupIds, string keywords, int pageIndex, int pageSize, out int pageCount);

        Document GetDocumentById(int id);
    }
}