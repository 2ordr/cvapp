﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class CandidateTagService : ICandidateTagService
    {

        public CandidateTag GetCandidateById(int id)
        {
            CandidateTag candidateTag = new CandidateTag();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from CandidateTags where Id=" + id)
                .MyExecuteReader(record =>
                {
                    candidateTag.Id = id;
                    candidateTag.CandidateId = record.SafeRead<int>("CandidateId");
                    candidateTag.TagId = record.SafeRead<int>("TagId");
                });

            return candidateTag;
        }

        public List<Models.CandidateTag> GetCandidateTags(int candidateId)
        {
            List<CandidateTag> result = new List<CandidateTag>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidateTags where candidateId={0}", candidateId))
                .MyExecuteReader(record =>
                {
                    result.Add(new CandidateTag
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        TagId = record.SafeRead<int>("TagId"),
                        ProfessionTag = new ProfessionTag
                        {
                            Id = record.SafeRead<int>("TagId"),
                            Tag = ADOAccess.MyExecuteScalar("Tag", "ProfessionTags", "Id=" + record.SafeRead<int>("TagId")).ToString()
                        }
                    });
                });

            return result;
        }

        public void AddNew(Models.CandidateTag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "CandidateTags", string.Format(" candidateId={0} and TagId={1}", entity.CandidateId, entity.TagId));

            if (exists != null)
                return;

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into CandidateTags(CandidateId,TagId) values({0},{1})",
                    entity.CandidateId,
                    entity.TagId))
                .MyExecuteNonQuery();


            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();
        }

        public void Update(Models.CandidateTag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "CandidateTags", string.Format(" candidateId={0} and TagId={1}", entity.CandidateId, entity.TagId));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Update CandidateTags set CandidateId={0}, TagId={1} where Id={2}",
                entity.CandidateId,
                entity.TagId,
                entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

        }

        public void Delete(Models.CandidateTag entity)
        {
            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidateTags where candidateId={0} and tagId={1}",
                entity.CandidateId,
                entity.TagId))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();
        }
    }
}