﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface ICandidateTagService : IServiceBase<CandidateTag>
    {

        List<CandidateTag> GetCandidateTags(int candidateId);

        CandidateTag GetCandidateById(int id);
    }
}