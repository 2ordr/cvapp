﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class FagService : IFagService
    {
        public void AddNew(Models.Fag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Fags", string.Format(" Fag='{0}'", entity.fag));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into Fags(Fag) values('{0}')",
                    entity.fag
                    ))
                .MyExecuteNonQuery();
        }
        public void Update(Models.Fag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Fags", string.Format(" Fag='{0}'", entity.fag));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Update Fags set Fag='{0}' where Id={1}",
                    entity.fag,
                    entity.Id
                    ))
                .MyExecuteNonQuery();
        }

        public void Delete(Models.Fag entity)
        {

            //var exists = ADOAccess.MyExecuteScalar("Id", "Fags", string.Format(" Id={0} ", entity.Id));

            //if (exists != null)
            //    throw new InvalidOperationException("Fjern børnegruppe");


            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from Fags where Id={0}",
                entity.Id))
            .MyExecuteNonQuery();
        }

        public List<Fag> GetAll()
        {
            List<Fag> result = new List<Fag>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from Fags order by Fag")
                .MyExecuteReader(record =>
                {
                    result.Add(new Fag
                    {
                        Id = record.SafeRead<int>("Id"),
                        fag = record.SafeRead<string>("Fag")

                    });
                });

            return result;
        }

        public Fag GetFagByID(int id)
        {
            Fag fag = new Fag();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Fags where Id=" + id)
                .MyExecuteReader(record =>
                {
                    fag.Id = id;
                    fag.fag = record.SafeRead<string>("Fag");

                });

            return fag;
        }

        public List<Fag> SearchFag(string keywords)
        {
            List<Fag> result = new List<Fag>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from Fags where Fag like '%' + '"  + keywords + "' + '%' order by Fag")
                .MyExecuteReader(record =>
                {
                    result.Add(new Fag
                    {
                        Id = record.SafeRead<int>("Id"),
                        fag = record.SafeRead<string>("Fag")

                    });
                });

            return result;
        }

    }
}