﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IRegionService
    {
        void AddNew(Region region);
        void Update(Region region);

        void Delete(Region region);

        IList<Region> GetFiletered(string sql, params DbParameter[] parameters);


        IList<Region> FilterPanelList();

        ZipCode GetZipCodeById(int id);

        ZipCode GetZipDetailsFromZipcode(string zipcode);

        List<Region> GetAll();

        Region GetRegionsByID(int id);

        List<City> GetCityDetailsByID(int id);

        List<ZipCode> GetZipCodeDetailsByCityID(int id);


    }
}