﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IServiceBase<T>
    {
        void AddNew(T entity);
        void Update(T entity);

        void Delete(T entity);
    }
}