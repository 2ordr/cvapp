﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IProfessionTagService : IServiceBase<ProfessionTag>
    {
        void AddNew(ProfessionTag professionTag);
        void Update(ProfessionTag professionTag);

        void Delete(ProfessionTag professionTag);

        IList<ProfessionTag> GetTopTags(int topCount);

        IList<ProfessionTag> GetAll();

        List<ProfessionTag> GetAllTags();

        ProfessionTag GetProfessionTagByID(int id);

    }
}