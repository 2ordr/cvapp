﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class DocumentGroupService : IDocumentGroupService
    {
  

        public List<Models.DocumentGroup> GetAll()
        {
            List<DocumentGroup> result = new List<DocumentGroup>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from DocumentGroups"))
                .MyExecuteReader(record =>
                {
                    result.Add(new DocumentGroup
                    {
                        Id = record.SafeRead<int>("Id"),
                        Name = record.SafeRead<string>("Name"),
                        Description = record.SafeRead<string>("Description"),
                        ParentGroupId = record.SafeRead<int>("ParentGroupId"),

                    });
                });

            return result;
        }

        public Models.DocumentGroup GetDocumentGroupById(int id)
        {
            DocumentGroup documentGroup = new DocumentGroup();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from DocumentGroups where Id=" + id)
                .MyExecuteReader(record =>
                {
                    documentGroup.Id = id;
                    documentGroup.Name = record.SafeRead<string>("Name");
                    documentGroup.Description = record.SafeRead<string>("Description");
                    documentGroup.ParentGroupId = record.SafeRead<int>("ParentGroupId");
                });

            return documentGroup;
        }

        public void AddNew(Models.DocumentGroup entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "DocumentGroups", string.Format(" ParentGroupId={0} and Name='{1}'", entity.ParentGroupId, entity.Name));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into DocumentGroups(Name,Description,ParentGroupId) values('{0}','{1}',{2})",
                    entity.Name,
                    entity.Description,
                    entity.ParentGroupId))
                .MyExecuteNonQuery();
        }

        public void Update(Models.DocumentGroup entity)
        {

            if (entity.Id == entity.ParentGroupId)
                throw new InvalidOperationException("Kontroller navnet på Parent");

            var exists = ADOAccess.MyExecuteScalar("Id", "DocumentGroups", string.Format(" ParentGroupId={0} and Name='{1}' and id<>{2} ", entity.ParentGroupId, entity.Name,entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Update DocumentGroups set Name='{0}',Description='{1}',ParentGroupId={2} where id={3} ",
                    entity.Name,
                    entity.Description,
                    entity.ParentGroupId,
                    entity.Id))
                .MyExecuteNonQuery();
        }

        public void Delete(Models.DocumentGroup entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "DocumentGroups", string.Format(" ParentGroupId={0} ", entity.Id));
            
            if (exists != null)
                throw new InvalidOperationException("Fjern børnegruppe");

            var docs = ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from documents where documentGroupId={0}", entity.Id))
                .MyExecuteScalar();

            if(docs!=null)
                throw new InvalidOperationException("Fjern børnegruppe");


            ADOAccess.GetDatabaseCommand()
                .SetSql("Delete from documentGroups where id=" + entity.Id)
                .MyExecuteNonQuery();

            
        }
    }
}