﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface ICandidateFileService : IServiceBase<CandidateFile>
    {
        List<CandidateFile> GetCandidateFiles(int candidateId);
        CandidateFile GetCandidateFilesbyID(int candidateId);

        List<CandidateFile> GetAll();
    
    }
}