﻿using CVApp2.Data;
using CVApp2.Models;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class CandidateNotesService : ICandidateNotesService
    {
         string connectionString;

        public CandidateNotesService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }


        public void Update(Models.CandidateNotes candidateNotes)
        {
            throw new NotImplementedException();
        }

        public void Delete(Models.CandidateNotes candidateNotes)
        {
            var modified = ADOAccess.GetDatabaseCommand()
              .SetSql(string.Format("Delete from CandidateNotes where Id={0}",
           
              candidateNotes.Id
              ))
           
              .MyExecuteScalar();


            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + candidateNotes.CandidateId)
                .MyExecuteNonQuery();

          
        }

        public List<CandidateNotes> GetCandidateNotes(int candidateId)
        {
            List<CandidateNotes> result = new List<CandidateNotes>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidateNotes where candidateId={0} order by Date desc", candidateId))
                .MyExecuteReader(record =>
                {
                    result.Add(new CandidateNotes
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        Note = record.SafeRead<string>("Note"),
                        Date = record.SafeRead<DateTime>("Date"),
                        UserName = record.SafeRead<string>("UserName")
                    });
                });

            return result;
        }

        public void AddNew(Models.CandidateNotes candidateNotes)
        {

         var modified = ADOAccess.GetDatabaseCommand()
                 .SetSql(string.Format("Insert into CandidateNotes(CandidateId,Note,Date,UserName)  " +
                                                     "  values('{0}',@note,getdate(),'{3}')",
                 candidateNotes.CandidateId,
                 candidateNotes.Note,
                 candidateNotes.Date,
                 candidateNotes.UserName
                 ))
                 .AddParameter("note",candidateNotes.Note)
                 .MyExecuteScalar();

         ADOAccess.GetDatabaseCommand()
             .SetSql("Update Candidates set LastUpdated=getdate() where id=" + candidateNotes.CandidateId)
             .MyExecuteNonQuery();

        }

        public CandidateNotes GetCandidateNotesById(int id)
        {
            CandidateNotes candidateNotes = new CandidateNotes();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from CandidateNotes where Id=" + id)
                .MyExecuteReader(record =>
                {
                    candidateNotes.Id = id;
                    candidateNotes.CandidateId = record.SafeRead<int>("CandidateId");
                    candidateNotes.Note = record.SafeRead<string>("Note");
                    candidateNotes.Date = record.SafeRead<DateTime>("Date");
                    candidateNotes.UserName = record.SafeRead<string>("UserName");
                });

            return candidateNotes;
        }
    
    }
}