﻿using CVApp2.Models;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface ICandidateNotesService : IServiceBase<CandidateNotes>
    {

        List<CandidateNotes> GetCandidateNotes(int id);

        CandidateNotes GetCandidateNotesById(int id);
    }
}