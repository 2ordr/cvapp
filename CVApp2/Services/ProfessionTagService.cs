﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class ProfessionTagService : IProfessionTagService
    {
        public IList<Models.ProfessionTag> GetTopTags(int topCount)
        {
            List<ProfessionTag> result = new List<ProfessionTag>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select Id,Tag from ProfessionTags")
                .ExecuteReader(record =>
                {
                    result.Add(new ProfessionTag
                    {
                        Id = (int)record.GetValue(0),
                        Tag = (string)record.GetValue(1)
                    });
                });

            return result;
        }


        public IList<ProfessionTag> GetAll()
        {

            return GetTopTags(int.MaxValue);
        }

        public void AddNew(Models.ProfessionTag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "ProfessionTags", string.Format(" Tag='{0}'", entity.Tag));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values('{0}')",
                    entity.Tag
                    ))
                .MyExecuteNonQuery();
        }

        public void Update(Models.ProfessionTag entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "ProfessionTags", string.Format(" Tag='{0}'", entity.Tag));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Update ProfessionTags set Tag='{0}' where Id={1}",
                    entity.Tag,
                    entity.Id
                    ))
                .MyExecuteNonQuery();
        }

        public void Delete(Models.ProfessionTag entity)
        {

            var exists = ADOAccess.MyExecuteScalar("Id", "CandidateTags", string.Format(" TagId={0} ", entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Fjern børnegruppe");


            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from ProfessionTags where Id={0}",
                entity.Id))
            .MyExecuteNonQuery();
        }

        public List<ProfessionTag> GetAllTags()
        {
            List<ProfessionTag> result = new List<ProfessionTag>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from ProfessionTags order by Tag")
                .MyExecuteReader(record =>
                {
                    result.Add(new ProfessionTag
                    {
                        Id = record.SafeRead<int>("Id"),
                        Tag = record.SafeRead<string>("Tag")

                    });
                });

            return result;
        }

        public ProfessionTag GetProfessionTagByID(int id)
        {
            ProfessionTag professionTag = new ProfessionTag();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from ProfessionTags where Id=" + id)
                .MyExecuteReader(record =>
                {
                    professionTag.Id = id;
                    professionTag.Tag = record.SafeRead<string>("Tag");

                });

            return professionTag;
        }
    }


}