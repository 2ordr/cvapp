﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class ZipCodeService : IZipCodeService
    {

        ICityService _cityService;

        public ZipCodeService()
        {
            _cityService = new CityService();
        }

        public void AddNew(Models.ZipCode entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Zipcodes", string.Format("Zipcode='{0}'", entity.ZipCodeNumber));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into Zipcodes(ZipCode,CityId) values ('{0}',{1})",
                    entity.ZipCodeNumber,
                    entity.CityId
                  ))
                .MyExecuteNonQuery();
        }

        public void Update(Models.ZipCode entity)
        {

            var exists = ADOAccess.MyExecuteScalar("Id", "Zipcodes", string.Format("Zipcode='{0}' and id<> {1} ", entity.ZipCodeNumber,entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
          .SetSql(string.Format("Update ZipCodes set ZipCode='{0}', CityId={1} where Id={2}",            
              entity.ZipCodeNumber,
              entity.CityId,
              entity.Id))
          .MyExecuteNonQuery();
        }

        public void Delete(Models.ZipCode entity)
        {

            var exists = ADOAccess.MyExecuteScalar("Id", "Candidates", string.Format(" ZipcodeId={0} ", entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Records eksisterer");

            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from Zipcodes where Id={0}",
               entity.Id))
           .MyExecuteNonQuery();
        }

        public ZipCode GetZipCodesByID(int id)
        {
            ZipCode zipcode = new ZipCode();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Zipcodes where Id=" + id)
                .MyExecuteReader(record =>
                {
                    zipcode.Id = id;
                    zipcode.ZipCodeNumber = record.SafeRead<string>("ZipCode");
                    zipcode.CityId = record.SafeRead<int>("CityId");
                    zipcode.City = _cityService.GetCityByID(record.SafeRead<int>("CityId"));
                });

            return zipcode;
        }

    }
}