﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;


namespace CVApp2.Services
{
    public class CandidateFileService : ICandidateFileService
    {

        public List<Models.CandidateFile> GetAll()
        {
            List<CandidateFile> result = new List<CandidateFile>();

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidateFiles " ))
                .MyExecuteReader(record =>
                {
                    result.Add(new CandidateFile
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        FileName = record.SafeRead<string>("FileName")
                        //not reading file content
                    });
                });

            return result;


        }

        public List<Models.CandidateFile> GetCandidateFiles(int candidateId)
        {
            List<CandidateFile> result = new List<CandidateFile>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidateFiles where candidateId={0}", candidateId))
                .MyExecuteReader(record =>
                {
                    result.Add(new CandidateFile
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        FileName = record.SafeRead<string>("FileName")
                        //not reading file content
                    });
                });

            return result;
        }

        public void AddNew(Models.CandidateFile entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "CandidateFiles", string.Format(" candidateId={0} and FileName='{1}' ", entity.CandidateId, entity.FileName));

            if (exists == null) //if not exists then only add else ignore
            {

                ADOAccess.GetDatabaseCommand()
                    .SetSql(string.Format("Insert into CandidateFiles(CandidateId,FileName,FileContent) values({0},'{1}','{2}')",
                        entity.CandidateId,
                        entity.FileName,
                        entity.FileContent.Replace("'","''")))
                    .MyExecuteNonQuery();


                ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

            }
        }

        public void Update(Models.CandidateFile entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Models.CandidateFile entity)
        {
            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from CandidateFiles where Id={0}",
               entity.Id))
           .MyExecuteNonQuery();


            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

        }

        public CandidateFile GetCandidateFilesbyID(int id)
        {
            CandidateFile candidateFile = new CandidateFile();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from CandidateFiles where Id=" + id)
                .MyExecuteReader(record =>
                {
                    candidateFile.Id = id;
                    candidateFile.CandidateId = record.SafeRead<int>("CandidateId");                  
                    candidateFile.FileName = record.SafeRead<string>("FileName");
                });

            return candidateFile;
        }

     
    }
}