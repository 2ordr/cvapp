﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class CityService : ICityService
    {

        string connectionString ; 
        

        public CityService()
        {
            //todo: remove hardcoded connection string
            connectionString=ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            
        }

        public void AddNew(Models.City entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Cities", string.Format("City='{0}'", entity.CityName));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into Cities(City,RegionId,ZipCode) values ('{0}',{1},'{2}')",
                    entity.CityName,
                    entity.RegionId,
                    entity.ZipCode
                  ))
                .MyExecuteNonQuery();
        }

        public void Update(Models.City entity)
        {
            ADOAccess.GetDatabaseCommand()
          .SetSql(string.Format("Update Cities set City='{0}',RegionId={1}, ZipCode='{2}' where Id={3}",
              entity.CityName,
              entity.RegionId,
              entity.ZipCode,
              entity.Id))
          .MyExecuteNonQuery();
        }

        public void Delete(Models.City entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Zipcodes", string.Format(" CityId={0} ", entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Fjern børnegruppe");

            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from Cities where Id={0}",
               entity.Id))
           .MyExecuteNonQuery();
        }

        public City GetCityByID(int id)
        {
           City city = new City();       

            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Cities where Id=" + id)
                .MyExecuteReader(record =>
                {
                    city.Id =id;
                    city.CityName = record.SafeRead<string>("City");
                    city.ZipCode = record.SafeRead<string>("ZipCode");
                    city.RegionId = record.SafeRead<int>("RegionId");

                    });

            Region region = new Region();
            ADOAccess.GetDatabaseCommand()
                           .SetSql("select top 1 * from Regions where Id=" + city.RegionId)
                           .MyExecuteReader(record =>
                           {
                               region.Id = record.SafeRead<int>("Id");
                               region.RegionName = record.SafeRead<string>("RegionName");

                           });

            city.Region = region;
            return city;
        }

        public List<ZipCode> GetZipCodeDetailsByCity(int id)
        {
            List<ZipCode> result = new List<ZipCode>();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select z.Id,z.ZipCode,c.City from Zipcodes z inner join Cities c on c.id= z.CityId where c.Id=" + id)
                .MyExecuteReader(record =>
                {
                    result.Add(new ZipCode
                    {
                        Id = record.SafeRead<int>("Id"),
                        ZipCodeNumber = record.SafeRead<string>("Zipcode")

                    });




                });

            return result;
        }
    }
}