﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface ICityService
    {
        void AddNew(City city);
        void Update(City city);

        void Delete(City city);

       City GetCityByID(int id);

       List<ZipCode> GetZipCodeDetailsByCity(int id);
    }
}