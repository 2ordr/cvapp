﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IZipCodeService
    {

        void AddNew(ZipCode zipcode);
        void Update(ZipCode zipcode);

        void Delete(ZipCode zipcode);

        ZipCode GetZipCodesByID(int id);

    }
}