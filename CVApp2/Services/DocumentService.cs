﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class DocumentService : IDocumentService
    {
        public List<Models.Document> GetAll()
        {
            List<Document> result = new List<Document>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from Documents"))
                .MyExecuteReader(record =>
                {
                    result.Add(new Document
                    {
                        Id = record.SafeRead<int>("Id"),
                        FileName = record.SafeRead<string>("FileName"),
                        DocumentGroupId = record.SafeRead<int>("DocumentGroupId"),
                        Created = record.SafeRead<DateTime>("Created"),
                        Tags = record.SafeRead<string>("Tags"),
                        ServerFileName = record.SafeRead<string>("ServerFileName"),

                        JNo = record.SafeRead<string>("JNo"),
                        JDate=record.SafeRead<DateTime>("JDate"),
                        ReleasedForPublication = record.SafeRead<string>("ReleasedForPublication"),
                        KarnovNo = record.SafeRead<string>("KarnovNo"),
                        KarnovDate = record.SafeRead<DateTime>("KarnovDate"),
                        TBBNo = record.SafeRead<string>("TBBNo"),
                        TBBDate = record.SafeRead<DateTime>("TBBDate"),
                        Summary = record.SafeRead<string>("Summary"),
                        Comments=record.SafeRead<string>("Comments"),
                        ArbitrationCourtComposition = record.SafeRead<string>("ArbitrationCourtComposition")

                    });
                });

            return result;
        }

        public List<Document> GetFiltered(string groupIds, string keywords, int pageIndex, int pageSize, out int pageCount)
        {
            List<Document> result = new List<Document>();
            var sqlCommand = ADOAccess.GetDatabaseCommand();
            sqlCommand.CommandText = "SearchDocuments";

            sqlCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter keywordsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@keywords", Value = keywords };
            SqlParameter regionIdsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@groupIds", Value = groupIds };



            SqlParameter pageIndexParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageIndex", Value = pageIndex };
            SqlParameter pageSizeParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageSize", Value = pageSize };

            SqlParameter pageCountParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageCount", Value = groupIds, Direction = ParameterDirection.Output };
            SqlParameter recordCountParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@recordCount", Value = groupIds, Direction = ParameterDirection.Output };


            sqlCommand.Parameters.Add(keywordsParam);
            sqlCommand.Parameters.Add(regionIdsParam);


            sqlCommand.Parameters.Add(pageIndexParam);
            sqlCommand.Parameters.Add(pageSizeParam);

            sqlCommand.Parameters.Add(pageCountParam);
            sqlCommand.Parameters.Add(recordCountParam);


            sqlCommand.MyExecuteReader(record =>
        {
            Document document = new Document
            {
                Id = record.SafeRead<int>("Id"),
                FileName = record.SafeRead<string>("FileName"),
                DocumentGroupId = record.SafeRead<int>("DocumentGroupId"),
                Created = record.SafeRead<DateTime>("Created"),
                Tags = record.SafeRead<string>("Tags"),
                ServerFileName = record.SafeRead<string>("ServerFileName"),

                JNo = record.SafeRead<string>("JNo"),
                JDate=record.SafeRead<DateTime>("JDate"),
                ReleasedForPublication = record.SafeRead<string>("ReleasedForPublication"),
                KarnovNo = record.SafeRead<string>("KarnovNo"),
                KarnovDate = record.SafeRead<DateTime>("KarnovDate"),
                TBBNo = record.SafeRead<string>("TBBNo"),
                TBBDate = record.SafeRead<DateTime>("TBBDate"),
                Summary = record.SafeRead<string>("Summary"),
                ArbitrationCourtComposition = record.SafeRead<string>("ArbitrationCourtComposition")


            };

            DocumentGroup group = new DocumentGroup();

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select top 1 * from DocumentGroups where Id ={0}", record.SafeRead<int>("DocumentGroupId")))
                .MyExecuteReader(record2 =>
                {
                    group.Description = record2.SafeRead<string>("Description");
                    group.Name = record2.SafeRead<string>("Name");
                    group.ParentGroupId = record2.SafeRead<int>("ParentGroupId");
                });

            document.DocumentGroup = group;
            result.Add(document);
        });

            pageCount = (int)pageCountParam.Value;
            // recordCount = (int)recordCountParam.Value;

            return result;
        }

        public Models.Document GetDocumentById(int id)
        {
            Document document = new Document();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Documents where Id=" + id)
                .MyExecuteReader(record =>
                {
                    document.Id = id;
                    document.FileName = record.SafeRead<string>("FileName");
                    document.DocumentGroupId = record.SafeRead<int>("DocumentGroupId");
                    document.ServerFileName = record.SafeRead<string>("ServerFileName");
                    document.FileContent = record.SafeRead<string>("FileContent");
                    document.Tags = record.SafeRead<string>("Tags");
                    document.Created = record.SafeRead<DateTime>("Created");

                    document.JNo = record.SafeRead<string>("JNo");
                    document.JDate = record.SafeRead<DateTime?>("Jdate");
                    document.ReleasedForPublication = record.SafeRead<string>("ReleasedForPublication");
                    document.KarnovNo = record.SafeRead<string>("KarnovNo");
                    document.KarnovDate = record.SafeRead<DateTime?>("KarnovDate");
                    document.TBBNo = record.SafeRead<string>("TBBNo");
                    document.TBBDate = record.SafeRead<DateTime?>("TBBDate");
                    document.Summary = record.SafeRead<string>("Summary");
                    document.ArbitrationCourtComposition = record.SafeRead<string>("ArbitrationCourtComposition");

                });

            return document;
        }

        public void AddNew(Models.Document entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Documents", string.Format("DocumentGroupId={0} and FileName='{1}'", entity.DocumentGroupId, entity.FileName));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into Documents(FileName,DocumentGroupId,Created,Tags,FileContent,ServerFileName,JNo,JDate,ReleasedForPublication,KarnovNo,KarnovDate,TBBNo,TBBDate,Summary,ArbitrationCourtComposition) values('{0}','{1}',{2},'{3}','{4}','{5}','{6}',{7},'{8}','{9}',{10},'{11}',{12},{13},'{14}')",
                    entity.FileName,
                    entity.DocumentGroupId,
                    ADOAccessExtensions.SafeInsert(entity.Created),
                    entity.Tags,
                    entity.FileContent,
                    entity.ServerFileName,
                    entity.JNo,
                    ADOAccessExtensions.SafeInsert(entity.JDate),
                    entity.ReleasedForPublication,
                    entity.KarnovNo,
                    ADOAccessExtensions.SafeInsert(entity.KarnovDate),
                    entity.TBBNo,
                    ADOAccessExtensions.SafeInsert(entity.TBBDate),
                    ADOAccessExtensions.SafeInsert(entity.Summary),
                    entity.ArbitrationCourtComposition))
                .MyExecuteNonQuery();
        }

        public void Update(Models.Document entity)
        {
            ADOAccess.GetDatabaseCommand()
          .SetSql(string.Format("Update Documents set FileName='{0}',DocumentGroupId={1}, Tags='{2}',FileContent='{3}',ServerFileName='{4}',Created={5},JNo='{6}', JDate={7}, ReleasedForPublication='{8}',KarnovNo='{9}',KarnovDate={10},TBBNo='{11}',TBBDate={12},Summary={13},ArbitrationCourtComposition='{14}', Comments='{16}'  where Id={15}",
              entity.FileName,
              entity.DocumentGroupId,
              entity.Tags,
              entity.FileContent,
              entity.ServerFileName,
              ADOAccessExtensions.SafeInsert(entity.Created),
              entity.JNo,
              ADOAccessExtensions.SafeInsert(entity.JDate),
              entity.ReleasedForPublication,
              entity.KarnovNo,
              ADOAccessExtensions.SafeInsert(entity.KarnovDate),
              entity.TBBNo,
              ADOAccessExtensions.SafeInsert(entity.TBBDate),
              ADOAccessExtensions.SafeInsert(entity.Summary),
              entity.ArbitrationCourtComposition,
              entity.Id,
              entity.Comments))
          .MyExecuteNonQuery();
        }

        public void Delete(Models.Document entity)
        {
            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from Documents where Id={0}",
               entity.Id))
           .MyExecuteNonQuery();
        }
    }
}