﻿using CVApp2.Data;
using CVApp2.Models;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class CandidateService : ICandidateService
    {
        string connectionString;

        public CandidateService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }


        public List<Candidate> GetCandidates(DateType dateType, Tuple<DateTime, DateTime> dateRange)
        {
            List<Candidate> result = new List<Candidate>();
            
            ADOAccess.GetDatabaseCommand()
                    .SetSql(string.Format("select * from candidates where {0} between '{1}' and '{2}'", dateType,dateRange.Item1.ToString("yyyy-MM-dd"), dateRange.Item2.ToString("yyyy-MM-dd")))
                    .ExecuteReader(record =>
                    {
                        Candidate candidate = new Candidate();
                        candidate.Id = record.SafeRead<int>("Id");
                        candidate.FirstName = record.SafeRead<string>("FirstName");
                        candidate.MiddleName = record.SafeRead<string>("MiddleName");
                        candidate.LastName = record.SafeRead<string>("LastName");
                        candidate.RegistrationId = record.SafeRead<string>("RegistrationId");
                        candidate.Company = record.SafeRead<string>("Company");
                        candidate.Designation = record.SafeRead<string>("Designation");
                        candidate.PhoneHome = record.SafeRead<string>("PhoneHome");
                        candidate.PhoneWork = record.SafeRead<string>("PhoneWork");
                        candidate.PhoneMobile = record.SafeRead<string>("PhoneMobile");
                        candidate.Fax = record.SafeRead<string>("Fax");
                        candidate.Email = record.SafeRead<string>("Email");
                        candidate.Website = record.SafeRead<string>("Website");

                        candidate.Address = record.SafeRead<string>("Address");
                        candidate.ZipcodeId = record.SafeRead<int>("ZipcodeId");

                        candidate.Notes = record.SafeRead<string>("Notes");
                        candidate.Status = record.SafeRead<CandidateStatus>("Status");

                        candidate.Ratings = record.SafeRead<int>("Ratings");
                        candidate.Bygherre = record.SafeRead<bool>("Bygherre");
                        candidate.Rådgiver = record.SafeRead<bool>("Rådgiver");
                        candidate.Entreprenør = record.SafeRead<bool>("Entreprenør");

                       // candidate.Zipcode = (string)ADOAccess.MyExecuteScalar("Zipcode", "Zipcodes", "Id=" + candidate.ZipcodeId);

                        result.Add(candidate);
                    });
            return result;
        }


        public List<CandidateListDetails> SearchPaged(string keywords, string regionIds, string tagIds, string skills, string status, string fromDate, string toDate, DateType dateType, int pageIndex, int pageSize, out int pageCount, out int recordCount)
        {
            List<CandidateListDetails> result = new List<CandidateListDetails>();
            pageCount = 1;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SearchCV";
                    command.CommandType=CommandType.StoredProcedure;

                    SqlParameter keywordsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@keywords", Value = keywords };
                    SqlParameter regionIdsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@regionIds", Value = regionIds };
                    SqlParameter tagIdsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@tagIds", Value = tagIds };
                    SqlParameter skillsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@skills", Value = skills };
                    SqlParameter statusParam = new SqlParameter { DbType = DbType.String, ParameterName = "@status", Value = status };

                    SqlParameter fromParam = new SqlParameter { DbType = DbType.String, ParameterName = "@fromDate", Value = fromDate };
                    SqlParameter toParam = new SqlParameter { DbType = DbType.String, ParameterName = "@toDate", Value = toDate };
                    SqlParameter dateTypeParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@dateType", Value = dateType };



                    SqlParameter pageIndexParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageIndex", Value = pageIndex };
                    SqlParameter pageSizeParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageSize", Value = pageSize };

                    SqlParameter pageCountParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@pageCount", Value = regionIds, Direction=ParameterDirection.Output };
                    SqlParameter recordCountParam = new SqlParameter { DbType = DbType.Int32, ParameterName = "@recordCount", Value = regionIds, Direction = ParameterDirection.Output };


                    command.Parameters.Add(keywordsParam);
                    command.Parameters.Add(regionIdsParam);
                    command.Parameters.Add(tagIdsParam);
                    command.Parameters.Add(skillsParam);
                    command.Parameters.Add(statusParam);

                    command.Parameters.Add(fromParam);
                    command.Parameters.Add(toParam);
                    command.Parameters.Add(dateTypeParam);

                    command.Parameters.Add(pageIndexParam);
                    command.Parameters.Add(pageSizeParam);

                    command.Parameters.Add(pageCountParam);
                    command.Parameters.Add(recordCountParam);



                    connection.Open();
                    using (var reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            result.Add(new CandidateListDetails
                            {
                                Id = reader.SafeRead<int>("Id"),
                                FirstName = reader.SafeRead<string>("FirstName"),
                                MiddleName = reader.SafeRead<string>("MiddleName"),
                                LastName=reader.SafeRead<string>("LastName"),
                                RegistrationId=reader.SafeRead<string>("RegistrationId"),

                                PhoneHome=reader.SafeRead<string>("PhoneHome"),
                                PhoneWork=reader.SafeRead<string>("PhoneWork"),
                                PhoneMobile=reader.SafeRead<string>("PhoneMobile"),

                                Address=reader.SafeRead<string>("Address"),
                                Notes=reader.SafeRead<string>("Notes"),

                                Company=reader.SafeRead<string>("Company"),
                                Designation=reader.SafeRead<string>("Designation"),

                                Website=reader.SafeRead<string>("Website"),

                                Status=reader.SafeRead<CandidateStatus>("Status"),

                                LastUpdated = reader.SafeRead<DateTime>("LastUpdated"),

                                SkillsCSV=reader.SafeRead<string>("skillsCSV"),

                                City=reader.SafeRead<string>("city"),
                                Zipcode=reader.SafeRead<string>("zipcode"),

                                Files=reader.SafeRead<string>("filesCSV"),
                                
                                Email=reader.SafeRead<string>("Email"),

                                Fax=reader.SafeRead<string>("Fax"),

                                State = reader.SafeRead<string>("Stat_Provins"),

                                Bygherre = reader.SafeRead<bool>("Bygherre"),

                                Rådgiver = reader.SafeRead<bool>("Rådgiver"),

                                Entreprenør = reader.SafeRead<bool>("Entreprenør"),

                                CandidateType = string.Format("{0} {1} {2}", reader.SafeRead<bool>("Bygherre")==true ? "B" : "",  reader.SafeRead<bool>("Rådgiver")==true ? "R" : "",reader.SafeRead<bool>("Entreprenør")==true ? "E" : "")
                                
                            });
                        }
                    }
                    pageCount = (int)pageCountParam.Value;
                    recordCount = (int)recordCountParam.Value;
                }
            }

            return result;
            
        }

        public List<string> AllSkills()
        {
            List<string> result = new List<string>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select distinct Skill from CandidateSkills order by 1")
                .ExecuteReader(record => 
                {
                    result.Add((string)record.GetValue(0));
                });

            return result;
        }


        public Candidate GetCandidate(int id)
        {
            Candidate result = new Candidate();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("select top 1 * from candidates where id={0}", id))
                .ExecuteReader(record => 
                {
                    
                        result.Id=id;
                        result.FirstName=record.SafeRead<string>("FirstName");
                        result.MiddleName = record.SafeRead<string>("MiddleName");
                        result.LastName =record.SafeRead<string>("LastName");
                        result.RegistrationId = record.SafeRead<string>("RegistrationId");
                        result.Company=record.SafeRead<string>("Company");
                        result.Designation=record.SafeRead<string>("Designation");
                        result.PhoneHome = record.SafeRead<string>("PhoneHome");
                        result.PhoneWork = record.SafeRead<string>("PhoneWork");
                        result.PhoneMobile = record.SafeRead<string>("PhoneMobile");
                        result.Fax = record.SafeRead<string>("Fax");
                        result.Email = record.SafeRead<string>("Email");
                        result.Website = record.SafeRead<string>("Website");

                        result.Address = record.SafeRead<string>("Address");
                        result.ZipcodeId = record.SafeRead<int>("ZipcodeId");

                        result.Notes = record.SafeRead<string>("Notes");
                        result.Status = record.SafeRead<CandidateStatus>("Status");

                        result.Ratings = record.SafeRead<int>("Ratings");
                        result.Bygherre = record.SafeRead<bool>("Bygherre");
                        result.Rådgiver = record.SafeRead<bool>("Rådgiver");
                        result.Entreprenør = record.SafeRead<bool>("Entreprenør");

                        result.Zipcode = (string) ADOAccess.MyExecuteScalar("Zipcode", "Zipcodes", "Id=" + result.ZipcodeId);



                    });
            UpdateDates(DateType.LastAccessed, id);
            return result;
                    
        }


        public void AddNew(Models.Candidate candidate)
        {

            var exists = ADOAccess.MyExecuteScalar("Zipcode", "Zipcodes", string.Format("Zipcode='{0}'", candidate.Zipcode));

            if (exists == null)
                throw new InvalidOperationException("Indtast korrekt postnummer");


            var modified = ADOAccess.GetDatabaseCommand()
                 .SetSql(@"Insert into candidates(FirstName,LastName,RegistrationId,PhoneHome,PhoneWork,PhoneMobile,Fax,Website,Email,Address,ZipcodeId,Notes,Status,Ratings,Company,Designation,Bygherre,Rådgiver,Entreprenør,Created,MiddleName) output INSERTED.ID 
                                                       values(@FirstName,@LastName,@RegistrationId,@PhoneHome,@PhoneWork,@PhoneMobile,@Fax,@Website,@Email,@Address,@ZipcodeId,@Notes,@Status,@Ratings,@Company,@Designation,@Bygherre,@Rådgiver,@Entreprenør,@Created,@MiddleName)")
                    .AddParameter("@FirstName", candidate.FirstName)
                    .AddParameter("@LastName", candidate.LastName)
                    .AddParameter("@RegistrationId", candidate.RegistrationId)
                    .AddParameter("@PhoneHome", candidate.PhoneHome)
                    .AddParameter("@PhoneWork", candidate.PhoneWork)
                    .AddParameter("@PhoneMobile", candidate.PhoneMobile)
                    .AddParameter("@Fax", candidate.Fax)
                    .AddParameter("@Website", candidate.Website)
                    .AddParameter("@Email", candidate.Email)
                    .AddParameter("@Address", candidate.Address)
                    .AddParameter("@ZipcodeId", candidate.ZipcodeId)
                    .AddParameter("@Notes", candidate.Notes)
                    .AddParameter("@Status", candidate.Status)
                    .AddParameter("@Ratings", candidate.Ratings)
                    .AddParameter("@Company", candidate.Company)
                    .AddParameter("@Designation", candidate.Designation)
                    .AddParameter("@Bygherre", candidate.Bygherre)
                    .AddParameter("@Rådgiver", candidate.Rådgiver)
                    .AddParameter("@Entreprenør", candidate.Entreprenør)
                    .AddParameter("@Created", DateTime.Now)
                    .AddParameter("@MiddleName", candidate.MiddleName)
                    .MyExecuteNonQuery();

                 //candidate.FirstName,
                 //candidate.LastName,
                 //candidate.RegistrationId,
                 //candidate.PhoneHome,
                 //candidate.PhoneWork,
                 //candidate.PhoneMobile,
                 //candidate.Fax,
                 //candidate.Website,
                 //candidate.Email,
                 //candidate.Address,
                 //candidate.ZipcodeId,
                 //candidate.Notes,
                 //(int)candidate.Status,
                 //candidate.Ratings,
                 //candidate.Company,
                 //candidate.Designation,
                 //candidate.Bygherre,
                 //candidate.Rådgiver,
                 //candidate.Entreprenør,
                 //ADOAccessExtensions.SafeInsert(DateTime.Now),
                 //candidate.MiddleName
                 //))
                 //.MyExecuteScalar();


            //var modified = ADOAccess.GetDatabaseCommand()
            //     .SetSql(string.Format("Insert into candidates(FirstName,LastName,RegistrationId,PhoneHome,PhoneWork,PhoneMobile,Fax,Website,Email,Address,ZipcodeId,Notes,Status,Ratings,Company,Designation,Bygherre,Rådgiver,Entreprenør,Created,MiddleName) output INSERTED.ID" +
            //                                         "  values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},'{11}','{12}',{13},'{14}','{15}','{16}','{17}','{18}',{19},'{20}')",
            //     candidate.FirstName,
            //     candidate.LastName,
            //     candidate.RegistrationId,
            //     candidate.PhoneHome,
            //     candidate.PhoneWork,
            //     candidate.PhoneMobile,
            //     candidate.Fax,
            //     candidate.Website,
            //     candidate.Email,
            //     candidate.Address,
            //     candidate.ZipcodeId,
            //     candidate.Notes,
            //     (int)candidate.Status,
            //     candidate.Ratings,
            //     candidate.Company,
            //     candidate.Designation,
            //     candidate.Bygherre,
            //     candidate.Rådgiver,
            //     candidate.Entreprenør,
            //     ADOAccessExtensions.SafeInsert(DateTime.Now),
            //     candidate.MiddleName
            //     ))
            //     .MyExecuteScalar();

            candidate.Id = (int)modified;
        }

        public void Update(Models.Candidate candidate)
        {
            var exists = ADOAccess.MyExecuteScalar("Zipcode", "Zipcodes", string.Format("Zipcode='{0}'", candidate.Zipcode));

            if (exists == null)
                throw new InvalidOperationException("Indtast korrekt postnummer");

            ADOAccess.GetDatabaseCommand()
                .SetSql(@"Update candidates set 
                            FirstName=@fn, LastName=@ln,MiddleName=@mn,
                            RegistrationId=@regId,
                            PhoneHome=@phHome,PhoneWork=@phWork ,PhoneMobile=@phMobile,Fax=@fax,
                            Website=@web,Email=@email,
                            Address=@add,ZipcodeId=@zip,Notes=@notes,Status=@status,Ratings=@ratings,
                            Company=@comp,Designation=@dsg,
                            Bygherre=@byg,Rådgiver=@rad,Entreprenør=@ent,
                            LastUpdated=@updt
                    where Id= @id")
                .AddParameter("@fn", candidate.FirstName )
                .AddParameter("@ln", candidate.LastName )
                .AddParameter("@mn", candidate.MiddleName )
                .AddParameter("@regId", candidate.RegistrationId )
                .AddParameter("@phHome", candidate.PhoneHome )

                .AddParameter("@phWork", candidate.PhoneWork )
                .AddParameter("@phMobile", candidate.PhoneMobile )
                .AddParameter("@fax", candidate.Fax )
                .AddParameter("@web", candidate.Website )
                .AddParameter("@email", candidate.Email )
                .AddParameter("@add", candidate.Address )
                .AddParameter("@zip", candidate.ZipcodeId)
                .AddParameter("@notes", candidate.Notes )
                .AddParameter("@status", (int) candidate.Status)
                .AddParameter("@ratings", candidate.Ratings)
                .AddParameter("@comp", candidate.Company )
                .AddParameter("@dsg", candidate.Designation )
                .AddParameter("@byg", candidate.Bygherre)
                .AddParameter("@rad", candidate.Rådgiver)
                .AddParameter("@ent", candidate.Entreprenør)
                .AddParameter("@updt", DateTime.Now)
                .AddParameter("@id", candidate.Id)
                .MyExecuteNonQuery();

        }

        public void Delete(Models.Candidate entity)
        {


            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidateTags where CandidateId={0}",
            entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from CandidatePreferredRegions where CandidateId={0}",
           entity.Id))
           .MyExecuteNonQuery();


            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from CandidateFiles where CandidateId={0}",
           entity.Id))
           .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidateSkills where CandidateId={0}",
            entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidateNotes where CandidateId={0}",
            entity.Id))
            .MyExecuteNonQuery();


            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from Candidates where Id={0}",
                entity.Id))
            .MyExecuteNonQuery();

          
        }

        
        public Candidate GetCandidateDetails(int id)
        {
            Candidate result = new Candidate();
            List<string> skills = new List<string>();
            List<string> professiontags = new List<string>();
            List<string> PreferredRegions = new List<string>();
            List<string> CandidateFiles = new List<string>();

            ADOAccess.GetDatabaseCommand(true)
                .SetSql("GetCandidateDetailsByID")
                .AddParameter("@id",id)
                .ExecuteReader(record =>
                {

                    result.Id = id;
                    result.FirstName = record.SafeRead<string>("FirstName");
                    result.MiddleName = record.SafeRead<string>("MiddleName");

                    result.LastName = record.SafeRead<string>("LastName");
                    result.RegistrationId = record.SafeRead<string>("RegistrationId");

                    result.Company = record.SafeRead<string>("Company");
                    result.Designation = record.SafeRead<string>("Designation");
                    result.City = record.SafeRead<string>("City");
                    
                    result.PhoneHome = record.SafeRead<string>("PhoneHome");
                    result.PhoneWork = record.SafeRead<string>("PhoneWork");
                    result.PhoneMobile = record.SafeRead<string>("PhoneMobile");
                    result.Address = record.SafeRead<string>("Address");
                    result.Notes = record.SafeRead<string>("Notes");
                    result.Website = record.SafeRead<string>("Website");
                    result.Zipcode = record.SafeRead<string>("Zipcode");
                    result.Email = record.SafeRead<string>("Email");
                    result.Fax = record.SafeRead<string>("Fax");
                    result.State = record.SafeRead<string>("Stat_Provins");
                    result.Region = record.SafeRead<string>("Region");
                    result.Ratings = record.SafeRead<int>("Ratings");
                    result.Status = record.SafeRead<CandidateStatus>("Status");
                    result.Bygherre = record.SafeRead<bool>("Bygherre");
                    result.Rådgiver = record.SafeRead<bool>("Rådgiver");
                    result.Entreprenør = record.SafeRead<bool>("Entreprenør");

                    result.Created = record.SafeRead<DateTime>("Created");
                    result.LastUpdated = record.SafeRead<DateTime>("LastUpdated");
                    result.LastAccessed = record.SafeRead<DateTime>("LastAccessed");

                    skills.Add(record.SafeRead<string>("Skill"));
                    CandidateFiles.Add(record.SafeRead<string>("FileName"));
                    professiontags.Add(record.SafeRead<string>("Tag"));
                    PreferredRegions.Add(record.SafeRead<string>("Regionname"));
                    

                });

            result.Skills = skills.Distinct().ToList();
            result.CandidateFiles = CandidateFiles.Distinct().ToList();
            result.ProfessionTags = professiontags.Distinct().ToList();
            result.PreferredRegions = PreferredRegions.Where(reg => reg != null).Distinct().Select(x => new PreferredRegion {  Region = new Region { RegionName = x } }).ToList();

            UpdateDates(DateType.LastAccessed, id);
            return result;
            
        }

        public List<string> GetMatchedList(string keywords, string regionIds, string tagIds, string skills, bool? status)
        {
            List<string> result = new List<string>();

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SearchCVNames";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter keywordsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@keywords", Value = keywords };
                    SqlParameter regionIdsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@regionIds", Value = regionIds };
                    SqlParameter tagIdsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@tagIds", Value = tagIds };
                    SqlParameter skillsParam = new SqlParameter { DbType = DbType.String, ParameterName = "@skills", Value = skills };
                    SqlParameter statusParam = new SqlParameter { DbType = DbType.Boolean, ParameterName = "@status", Value = status };

                    command.Parameters.Add(keywordsParam);
                    command.Parameters.Add(regionIdsParam);
                    command.Parameters.Add(tagIdsParam);
                    command.Parameters.Add(skillsParam);
                    command.Parameters.Add(statusParam);

                    connection.Open();
                    using (var reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {

                            {
                                //result.Add((string)reader.GetValue(0));

                                result.Add( reader.SafeRead<string>("Name"));
                            }
                        }
                    }
                }
            }


                     
            return result;
        }


        public List<Object> CustomersFromSharepoint(string registrationNumber)
        {
            List<Object> result = new List<Object>();
            

                ADOAccess.GetDatabaseCommand("ExformaticsDbConnection")
                    .SetSql("select top 30 VBA_id,VBA_nam from VBA_prs where cast(vba_id as nvarchar) like '%" + registrationNumber +"%'")
                    .ExecuteReader(record =>
                    {
                        string no = record.GetValue(0).ToString();
                        string name = (string)record.GetValue(1);
                        result.Add(new { label = string.Format("{0:6} {1}", no, name), value = no }); // (string)record.GetValue(0));
                    });

            return result;
        }


        public Candidate CustomerFromSharepoint(string registrationNumber)
        {
            Candidate result = new Candidate();

            int regNo = 0;
            int.TryParse(registrationNumber, out regNo);

            if (regNo == 0)
                return result;


            ADOAccess.GetDatabaseCommand("ExformaticsDbConnection")
                    .SetSql("select top 1 * from VBA_prs where vba_id =" + regNo )
                    .ExecuteReader(record =>
                    {

                        //split name into first and last name
                        var str = record.SafeRead<string>("VBA_nam");

                        if (!string.IsNullOrWhiteSpace(str) && str.Contains(' '))
                        {
                           var names = str.Split(' ');
                           result.FirstName = names[0];
                           result.MiddleName = string.Join(" ", names, 1, names.Length - 2);

                           result.LastName = names[names.Length-1];
                        }
                        else
                        {
                            result.FirstName = str;
                        }

                        result.RegistrationId = registrationNumber;
                       // result.Company = record.SafeRead<string>("Company");
                        result.Designation = record.SafeRead<string>("VBA_titel");
                        result.PhoneWork = record.SafeRead<string>("DL_phone");
                        result.PhoneMobile = record.SafeRead<string>("DL_phone");
                        result.Fax = record.SafeRead<string>("DL_Fax");
                        result.Email = record.SafeRead<string>("DL_Email");
                        result.Address = string.Format ("{0} {1}", record.SafeRead<string>("DL_Street"), record.SafeRead<string>("DL_Street2"));
                        result.Zipcode = record.SafeRead<string>("DL_PostalCode");
                        
                    });


            return result;
        }

        public void UpdateDates(DateType type, int candidateId) {
            string fieldname = "LastUpdated";
            switch (type)
            {
                case DateType.Created:
                    fieldname = "Created";
                    break;
                case DateType.LastUpdated:
                    fieldname = "LastUpdated";
                    break;
                case DateType.LastAccessed:
                default:
                    fieldname = "LastAccessed";
                    break;
            }

            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Update candidates set {0}={1} where Id={2}",
            fieldname,
            ADOAccessExtensions.SafeInsert(DateTime.Now),
            candidateId))
            .MyExecuteNonQuery();
        }

    }

    
}