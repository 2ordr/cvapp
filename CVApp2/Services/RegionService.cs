﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class RegionService : IRegionService
    {
        
        string connectionString ; 
        

        public RegionService()
        {
            //todo: remove hardcoded connection string
            connectionString=ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            
        }


        public void AddNew(Models.Region entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Regions", string.Format("RegionName='{0}'", entity.RegionName));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into Regions(RegionName) values ('{0}')",
                    entity.RegionName
                  ))
                .MyExecuteNonQuery();
        }

        public void Update(Models.Region entity)
        {
            ADOAccess.GetDatabaseCommand()
          .SetSql(string.Format("Update Regions set RegionName='{0}' where Id={1}",
              entity.RegionName,            
              entity.Id))
          .MyExecuteNonQuery();
        }

        public void Delete(Models.Region entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "Cities", string.Format(" RegionId={0} ", entity.Id));

            if (exists != null)
                throw new InvalidOperationException("Fjern børnegruppe");

            ADOAccess.GetDatabaseCommand()
           .SetSql(string.Format("Delete from Regions where Id={0}",
               entity.Id))
           .MyExecuteNonQuery();
        }

        public IList<Models.Region> GetFiletered(string sql,params DbParameter[] parameters)
        {
            List<Region> result = new List<Region>();

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = sql;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);

                    connection.Open();
                    using (var reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            result.Add(new Region
                            {
                                Id = (int)reader["Id"],
                                RegionName = (string)reader["RegionName"]
                            });
                        }
                    }
                }
            }

            return result;
        }

        public IList<Region> FilterPanelList()
        {
            List<Region> result = new List<Region>();
                ADOAccess.GetDatabaseCommand()
                .SetSql("Select Id,RegionName,IsAutoSelected from regions order by DisplayOrder")
                //.AddParameter("showParam",1,System.Data.DbType.Byte)
                .ExecuteReader(record =>
                {
                    result.Add(new Region
                    {
                        Id = (int)record.GetValue(0),
                        RegionName = (string)record.GetValue(1),
                        IsAutoSelected=record.GetValue(2) is bool?(bool)record.GetValue(2):false
                    });
                });

            return result;
            
        }


        public ZipCode GetZipCodeById(int id)
        {
            ZipCode zipcode = new ZipCode();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Zipcodes where id=" + id)
                .MyExecuteReader(record =>
                {
                    zipcode.Id = id;
                    zipcode.CityId= record.SafeRead<int>("CityId");
                    zipcode.ZipCodeNumber=record.SafeRead<string>("Zipcode");
                });

            return zipcode;
        }

        public ZipCode GetZipDetailsFromZipcode(string zipcode)
        {
            ZipCode result = new ZipCode();

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("select top 1 * from Zipcodes where Zipcode='{0}'", zipcode))
                .MyExecuteReader(record =>
                {
                    result.ZipCodeNumber = zipcode;
                    result.CityId= record.SafeRead<int>("CityId");
                    result.Id=record.SafeRead<int>("Id");
                    result.City = new City
                    {
                        Id = result.CityId,
                        CityName = (string)ADOAccess.MyExecuteScalar("City", "Cities", "Id=" + result.CityId),
                        RegionId = (int)ADOAccess.MyExecuteScalar("RegionId", "Cities", "Id=" + result.CityId)
                    };
                    result.City.Region = new Region { Id = result.City.RegionId, RegionName = (string)ADOAccess.MyExecuteScalar("RegionName", "Regions", "Id=" + result.City.RegionId) };

                });

            return result;
        }

        public List<Region> GetAll()
        {
            List<Region> result = new List<Region>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from Regions order by RegionName")
                .MyExecuteReader(record =>
                {
                    result.Add(new Region
                    {
                        Id = record.SafeRead<int>("Id"),
                        RegionName=record.SafeRead<string>("RegionName")

                    });
                });

            return result;
        }

        public Region GetRegionsByID(int id)
        {
            Region regions = new Region();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from Regions where Id=" + id)
                .MyExecuteReader(record =>
                {
                    regions.Id = id;
                    regions.RegionName = record.SafeRead<string>("RegionName");
                  
                });

            return regions;
        }

        public List<City> GetCityDetailsByID(int id)
        {
            List<City> result = new List<City>();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select c.Id,c.city,c.ZipCode,r.RegionName from Cities c inner join Regions R on c.RegionId= R.Id where R.Id=" + id + " order by c.City ")
                .MyExecuteReader(record =>
                {
                    result.Add(new City
                    {
                        Id = record.SafeRead<int>("Id"),
                        CityName = record.SafeRead<string>("City"),
                       ZipCode=record.SafeRead<string>("ZipCode")

                    });

                    
                  

                });

            return result;
        }

        public List<ZipCode> GetZipCodeDetailsByCityID(int id)
        {
            List<ZipCode> result = new List<ZipCode>();

            ADOAccess.GetDatabaseCommand()
                .SetSql("select z.Id,z.ZipCode,c.City from Zipcodes z inner join Cities c on c.id= z.CityId where c.Id=" + id)
                .MyExecuteReader(record =>
                {
                    result.Add(new ZipCode
                    {
                        Id = record.SafeRead<int>("Id"),
                        ZipCodeNumber = record.SafeRead<string>("Zipcode")

                    });




                });

            return result;
        }

    }
}