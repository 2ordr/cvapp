﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface IPreferredRegionService : IServiceBase<PreferredRegion>
    {
        List<PreferredRegion> GetAll();

        PreferredRegion GetPreferredRegion(int id);

        List<PreferredRegion> GetCandidateRegions(int candidateId);
    }
}