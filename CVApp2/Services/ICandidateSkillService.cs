﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public interface ICandidateSkillService : IServiceBase<CandidateSkill>
    {
        List<CandidateSkill> GetCandidateSkills(int candidateId);

        CandidateSkill GetCandidateSkillsByID(int id);
    }
}