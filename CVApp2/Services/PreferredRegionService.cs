﻿using CVApp2.Data;
using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Services
{
    public class PreferredRegionService : IPreferredRegionService
    {
        public void AddNew(Models.PreferredRegion entity)
        {

            var exists = ADOAccess.MyExecuteScalar("Id", "CandidatePreferredRegions", string.Format(" candidateId={0} and RegionId={1}", entity.CandidateId, entity.RegionId));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},{1})",
                    entity.CandidateId,
                    entity.RegionId))
                .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

        }

        public void Update(Models.PreferredRegion entity)
        {
            var exists = ADOAccess.MyExecuteScalar("Id", "CandidatePreferredRegions", string.Format(" candidateId={0} and RegionId={1}", entity.CandidateId, entity.RegionId));

            if (exists != null)
                throw new InvalidOperationException("Eksisterer allerede");

            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Update CandidatePreferredRegions set CandidateId={0}, RegionId={1} where Id={2}",
                entity.CandidateId,
                entity.RegionId,
                entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();

        }

        public void Delete(Models.PreferredRegion entity)
        {
            ADOAccess.GetDatabaseCommand()
            .SetSql(string.Format("Delete from CandidatePreferredRegions where Id={0}",
                entity.Id))
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
                .SetSql("Update Candidates set LastUpdated=getdate() where id=" + entity.CandidateId)
                .MyExecuteNonQuery();
        }

        
        public List<PreferredRegion> GetAll()
        {
            List<PreferredRegion> result = new List<PreferredRegion>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from CandidatePreferredRegions")
                .MyExecuteReader(record =>
                {
                    result.Add(new PreferredRegion
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId=record.SafeRead<int>("CandidateId"),
                        RegionId=record.SafeRead<int>("RegionId")
                    });
                });

            return result;
        }

        public PreferredRegion GetPreferredRegion(int id)
        {
            PreferredRegion preferredRegion = new PreferredRegion();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select top 1 * from CandidatePreferredRegions where Id=" + id)
                .MyExecuteReader(record =>
                {
                    preferredRegion.Id=id;
                    preferredRegion.CandidateId=record.SafeRead<int>("CandidateId");
                    preferredRegion.RegionId = record.SafeRead<int>("RegionId");
                });

            return preferredRegion;
        }

        public List<PreferredRegion> GetCandidateRegions(int candidateId)
        {
            List<PreferredRegion> result = new List<PreferredRegion>();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Select * from CandidatePreferredRegions where candidateId={0}",candidateId) )
                .MyExecuteReader(record =>
                {
                    result.Add(new PreferredRegion
                    {
                        Id = record.SafeRead<int>("Id"),
                        CandidateId = record.SafeRead<int>("CandidateId"),
                        RegionId = record.SafeRead<int>("RegionId"),
                        Region = new Region 
                        { 
                            Id=record.SafeRead<int>("RegionId"),
                            RegionName=ADOAccess.MyExecuteScalar("RegionName","Regions","Id=" + record.SafeRead<int>("RegionId")).ToString()
                        }
                    });
                });

            return result;
        }
    }
}