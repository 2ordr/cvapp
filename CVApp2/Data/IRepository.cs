﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Data
{
    public interface IRepository<T> where T:BaseEntity
    {
        T GetById(object id);

        void Insert(T entity);


        void Update(T entity);


        void Delete(T entity);


        IList<T> SqlList(string sql); 

        IList<TEntity> StoredProcedureList<TEntity>(string command, params object[] parameters)
            where TEntity : BaseEntity, new();

    }
}