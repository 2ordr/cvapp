﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace CVApp2.Data
{


    public class ADOAccess
    {
        private static readonly string connectionString;

        static ADOAccess()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public static SqlCommand GetDatabaseCommand(bool IsStoredProcedure = false)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            if (IsStoredProcedure) command.CommandType = CommandType.StoredProcedure;
            return command;
        }

        public static SqlCommand GetDatabaseCommand(string connectionStringName, bool IsStoredProcedure = false)
        {
            string connStr = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            SqlConnection connection = new SqlConnection(connStr);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            if (IsStoredProcedure) command.CommandType = CommandType.StoredProcedure;
            return command;
        }


        public static object MyExecuteScalar(string selectField, string tableName, string condition)
        {
            return ADOAccess.GetDatabaseCommand().SetSql(string.Format("select {0} from {1} where {2}",
                selectField,
                tableName,
                condition
                )).MyExecuteScalar();
        }


    }


    public static class ADOAccessExtensions
    {

        private static Logger logger = LogManager.GetCurrentClassLogger(); 

        /// <summary>Sets the text command to run against the data source.</summary>
        /// <param name="databaseCommand"><see cref="DatabaseCommand" /> instance.</param>
        /// <param name="commandText">The text command to run against the data source.</param>
        /// <returns>The given <see cref="DatabaseCommand" /> instance.</returns>
        public static SqlCommand SetSql(this SqlCommand databaseCommand, string commandText)
        {
            databaseCommand.CommandText = commandText;
            return databaseCommand;
        }

        public static SqlCommand AddParameter(this SqlCommand databaseCommand, string parameterName, object parameterValue)
        {
            databaseCommand.Parameters.AddWithValue(parameterName, parameterValue??(object)DBNull.Value);
            return databaseCommand;
        }





        public static void ExecuteReader(this SqlCommand databaseCommand, Action<IDataRecord> dataRecordCallback, bool keepConnectionOpen = false)
        {
            try
            {

                databaseCommand.Connection.Open();

                using (DbDataReader dbDataReader = databaseCommand.ExecuteReader())
                {
                    while (dbDataReader.HasRows)
                    {
                        while (dbDataReader.Read())
                        {
                            dataRecordCallback.Invoke(dbDataReader);
                        }

                        dbDataReader.NextResult();
                    }
                }


            }
            catch (Exception exception)
            {
                logger.Error(exception, exception.Message);
                throw;
            }
            finally
            {
                if (keepConnectionOpen == false)
                {
                    databaseCommand = CloseAndDisposeCommand(databaseCommand);
                }
            }
        }

        private static SqlCommand CloseAndDisposeCommand(SqlCommand databaseCommand)
        {
            databaseCommand.Connection.Close();

            databaseCommand.Connection.Dispose();

            databaseCommand.Dispose();
            databaseCommand = null;
            return databaseCommand;
        }


        public static void MyExecuteReader(this SqlCommand databaseCommand, Action<IDataRecord> dataRecordCallback, bool keepConnectionOpen = false)
        {
            logger.Info("Command:" + databaseCommand.CommandText);
            ExecuteReader(databaseCommand, dataRecordCallback, keepConnectionOpen);

        }

        public static int MyExecuteNonQuery(this SqlCommand databaseCommand, bool keepConnectionOpen = false)
        {
            logger.Info("Command:" + databaseCommand.GetDebugCommandText());

            int numberOfRowsAffected;

            try
            {

                databaseCommand.Connection.Open();

                numberOfRowsAffected = databaseCommand.ExecuteNonQuery();


            }
            catch (Exception exception)
            {
                logger.Error(exception, exception.Message);
                throw;
            }
            finally
            {
                if (keepConnectionOpen == false)
                {
                    CloseAndDisposeCommand(databaseCommand);
                }
            }

            return numberOfRowsAffected;
        }

        public static object MyExecuteScalar(this SqlCommand databaseCommand, bool keepConnectionOpen = false)
        {
            logger.Info("Command:" + databaseCommand.CommandText);

            object returnValue;

            try
            {

                databaseCommand.Connection.Open();

                returnValue = databaseCommand.ExecuteScalar();

                if (returnValue == DBNull.Value)
                    returnValue = null;


            }
            catch (Exception exception)
            {
                logger.Error(exception, exception.Message);
                throw;
            }
            finally
            {
                if (keepConnectionOpen == false)
                {
                    CloseAndDisposeCommand(databaseCommand);
                }
            }

            return returnValue;
        }




        public static string GetDebugCommandText(this DbCommand dbCommand)
        {
            var sb = new StringBuilder();

            sb.AppendLine("/******** COMMAND & CONNECTION INFO ********/");
            sb.AppendLine("/*");
            sb.AppendLine("Application Name: " + AppDomain.CurrentDomain.FriendlyName);
            sb.AppendLine("Database Server: " + dbCommand.Connection.DataSource);
            sb.AppendLine("Database Name: " + dbCommand.Connection.Database);
            sb.AppendLine("Connection String: " + dbCommand.Connection.ConnectionString);
            sb.AppendLine("Connection State: " + dbCommand.Connection.State);
            sb.AppendLine("Command Timeout: " + dbCommand.CommandTimeout + " sec.");
            sb.AppendLine("Command Parameter Count: " + dbCommand.Parameters.Count);
            sb.AppendLine("Client Machine Name: " + Environment.MachineName);
            sb.AppendLine("Current DateTime: " + DateTime.Now);
            sb.AppendLine("*/");
            sb.AppendLine();

            string commandText = dbCommand.GetParameterReplacedCommandText();

            sb.AppendLine("/******** SQL QUERY ********/");
            sb.AppendLine();
            sb.AppendLine(commandText);
            sb.AppendLine();

            return sb.ToString();
        }

        public static string GetParameterReplacedCommandText(this DbCommand dbCommand)
        {
            string commandText = dbCommand.CommandText;

            foreach (DbParameter parameter in dbCommand.Parameters)
            {
                string replacementText = string.Format("'{0}' /* {1} */", parameter.Value, parameter.ParameterName);

                commandText = commandText.Replace(parameter.ParameterName, replacementText);
            }

            return commandText;
        }


        public static T SafeRead<T>(this SqlDataReader reader, string columnName)
        {
            var obj = reader[columnName];
            return (obj == DBNull.Value ? default(T) : (T)obj);
        }


        public static T SafeRead<T>(this IDataRecord reader, string columnName)
        {
            var obj = reader[columnName];
            return (obj == DBNull.Value ? default(T) : (T)obj);
        }

        public static Object SafeInsert(object obj)
        {
            if (obj==null  || obj == DBNull.Value )
                return "NULL";
            else if (obj is int)
                return obj.ToString();
            else if (obj is DateTime)
            {
                DateTime dt = (DateTime)obj;
                return string.Format("'{0}{1:D2}{2:D2}'", dt.Year, dt.Month, dt.Day);
            }
            else
                return "'" + obj.ToString().Replace("'", "''") + "'";

        }

    }
}