﻿
$(function () {

    $('body:not(:has("#ajaxDialog"))').append('<div id="ajaxDialog" style="min-width:500px;">Im added</div>');
    $("#ajaxDialog").dialog({ autoOpen: false, modal: true, closeText: "", hide: "fadeOut", title: "CVApp", width: "auto", position: { my: "center top", at: "center top+175", collision: "fit" } });


    $(".tagCheckbox").change(function () {
               
        $.ajax({
            cache:false,
            url: "/CandidateTag/AddDeleteTag",
            data: {
                candidateId: $(this).attr('candidateId'),
                tagId: $(this).attr('tagId'),
                add: $(this).is(':checked')
            },
            success: function (response) {
                
            },

            failure: function (response) {
                alert('Oops! something went wrong, failure: ' + response.d);
            },

            error: function (response) {
                if (response.statusText == 'abort') {
                    return;
                }
                alert('Oops! something went wrong, error: ' + response);
            }

        });

    });

    $("#zipCode").on('input', function (e) {
        $.ajax({
            type: "GET",
            url: "/Region/ZipCodeDetails",
            data: { zipcodestr: $(this).val() },
            success: function (response) {
                $("#regionDetailsContainer").html(response);
            },
            failure: function (response) {
                alert('Oops! something went wrong, failure: ' + response.d);
            },
            error: function (response) {
                if (response.statusText == 'abort') {
                    return;
                }
                alert('Oops! something went wrong, error: ' + response);
            }
        });
    });

    $("#zipCode").trigger("input");

    $("#addAttachmentFileInput").change(function () {

        var data = new FormData();
        data.append('candidateId', candidateId);
        jQuery.each(jQuery('#addAttachmentFileInput')[0].files, function (i, file) {
            data.append('file-' + i, file);
        });


        jQuery.ajax({
            url: '/CandidateFiles/Create',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                handleSuccessCandidateFile(data);
            },
            failure: function (response) {
                alert('Oops! something went wrong, failure: ' + response.d);
            },
            error: function (response) {
                if (response.statusText == 'abort') {
                    return;
                }
                alert('Oops! something went wrong, error: ' + response);
            }
        });
    });


    $("img").mouseover(function () {          
          
        $(this).css("cursor", "pointer");
    });

    $("img").mouseout(function () {
           
    });

    $("img").click(function () {
        $("img").unbind("mouseout mouseover");     
                  
        giveRating($(this), parseInt($(this).attr("ratingVal")));          
          
    });


});


function groupItemclick(obj)
{
        
    var text= $(obj).text().trim();
    $("#Skill").val(text);
  
}

function showDialog(data) {
    $("#ajaxDialog").dialog("open");

};

function closeDialog() {
    $("#ajaxDialog").dialog("close");
};

function handleSuccessPreferredRegion(data) {

    $("#preferredRegionsContainer").html(data);
    $("#ajaxDialog").dialog("close");
};

function handleSuccessCandidateTag(data) {

    $("#tagsContainer").html(data);
    $("#ajaxDialog").dialog("close");
};

function handleSuccessCandidateSkill(data) {

    $("#skillsContainer").html(data);
    $("#ajaxDialog").dialog("close");
};


function handleSuccessCandidateFile(data) {

    $("#filesContainer").html(data);
    $("#ajaxDialog").dialog("close");
};

function handleError(ajaxContext) {

    $("#ajaxDialog").html(ajaxContext.responseText);
};

function attachmentFileClick() {

    $("#addAttachmentFileInput").trigger('click');


};


function giveRating(img, ratingVal) {
        
    for (i = 1; i <= ratingVal; i++)        {
           
        img.attr("src", "/Content/images/FilledStar.png").prevAll("img").attr("src", "/Content/images/FilledStar.png");
        img.attr("src", "/Content/images/FilledStar.png").nextAll("img").attr("src", "/Content/images/EmptyStar.png");          

    }
    var ratings= ratingVal;     
          
    $("#rateid").val(ratings);

       
       
};

function HideRating(img, image) {            
    img.attr("src", "/Content/Images/" + image)
       .prevAll("img").attr("src", "/Content/Images/" + image);
};

function editSuccessHandler(response) {
    var containerId = "#detailsContainer" + response;
    $(containerId).load("/CV/Details/" + response + "?tm=" + new Date().getTime() );
    $("#editCandidateContainer").dialog("close");
}

function editFailureHandler(response) {
    $("#editCandidateContainer").html(response.responseText);
}

function editDialogClose() {
     $("#editCandidateContainer").dialog("close");
}

function closeDialog(obj) {

    $("#ajaxDialog").dialog("close");
    
}