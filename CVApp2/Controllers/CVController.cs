﻿using CVApp2.Data;
using CVApp2.Models;
using CVApp2.Services;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace CVApp2.Controllers
{
    public class CVController : Controller
    {
        IRegionService _regionService;
        ICandidateService _candidateService;
        IProfessionTagService _professionTagsService;
  


        public CVController()
        {
            _regionService = new RegionService();
            _candidateService = new CandidateService();
            _professionTagsService = new ProfessionTagService();
          
        }

        // GET: CV
        public ActionResult Index(int? id)
        {
            string regionIds;
            if (TempData["regionIds"] != null)
                regionIds = TempData["regionIds"] as string;

            int pageCount;
            string CV = System.Configuration.ConfigurationManager.AppSettings.Get("CVExternalPage");
            SearchVM searchVM = new SearchVM();
            searchVM.Regions = _regionService.FilterPanelList();
            searchVM.Tags = _professionTagsService.GetAll();
            searchVM.Skills = _candidateService.AllSkills();
            searchVM.CVUrl = CV;
            if(id.HasValue && id.Value>0)
            {
                ViewBag.DisplayedCandidate = _candidateService.GetCandidate(id.Value);
            }

            return View(searchVM);
        }


        public ActionResult SearchResumes(string keywords, string regionIds, string tagIds, string skills, int pageIndex, int pageSize,string status,string fromDate, string toDate, int dateType)
        {
            TempData["regionIds"] = regionIds;

            int pageCount=1;
            int recordCount=0;
            var cvlist = _candidateService.SearchPaged(keywords, regionIds, tagIds,skills,status, fromDate,toDate, (DateType) dateType, pageIndex, pageSize, out pageCount,out recordCount);
            return Json(new { pageCount = pageCount, resumes = cvlist,cvCount=recordCount });
        }


             
        public ActionResult GetList(string prefix, string regIds, string taIds, string skils, bool? stats)
        {
            var List = _candidateService.GetMatchedList(prefix, regIds, taIds, skils, stats);
            return Json(List, JsonRequestBehavior.AllowGet);

        }

        private Object InsertValue(object obj)
        {
            if (obj == DBNull.Value)
                return "NULL";
            else if (obj is int)
                return obj.ToString();
            else
                return "'" + obj.ToString().Replace("'","''")   + "'";

        }

        public T DBSafeRead<T>(object obj)
        {
            return (obj == DBNull.Value ? default(T) : (T)obj);
        }


        private void CreateRegions()
        {
            //To create zipcodes table
            ADOAccess.GetDatabaseCommand()
                .SetSql("Truncate Table Zipcodes; Select * from Old_region")
                .ExecuteReader(record =>
                {
                    try
                    {

                        var city = ADOAccess.GetDatabaseCommand()
                            .SetSql(string.Format("Select  Id from Cities where City='{0}'", (string)record["CITY"]))
                            .MyExecuteScalar();
                        if (city != null)
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into Zipcodes(CityId,Zipcode) values({0},'{1}')", (int)city, (string)record["ZIPCODE"]))
                                .MyExecuteNonQuery(false);
                        }
                    }
                    catch (Exception)
                    {

                    }

                });
        }

        public ActionResult DBUpdate()
        {
            // Fags table created using following query
            // Select Fid as Id, Flag as Fag into Fags into from [Data-import].dbo.Flags


            //  delete attached files
            // and documents folder first, manually

            string attachmentsFolderpath=@"..\AttachedFiles";
            string attachmentsFolderOnServer = Server.MapPath(attachmentsFolderpath);
            if (!System.IO.Directory.Exists(attachmentsFolderOnServer))
            {
                System.IO.Directory.CreateDirectory(attachmentsFolderOnServer);
            }


            string documentsFolderPath = @"..\DocumentFiles";
            string documentsFolderPathOnServer = Server.MapPath(documentsFolderPath);
            if (!System.IO.Directory.Exists(documentsFolderPathOnServer))
            {
                System.IO.Directory.CreateDirectory(documentsFolderPathOnServer);
            }


            // To initialize database
            ADOAccess.GetDatabaseCommand()
            .SetSql("Delete from CandidatePreferredRegions;  DBCC CHECKIDENT ('CandidatePreferredRegions', RESEED, 0); Delete from CandidateFiles; DBCC CHECKIDENT ('CandidateFiles', RESEED, 0);  Delete from CandidateSkills;  DBCC CHECKIDENT ('CandidateSkills', RESEED, 0); Delete from CandidateTags; DBCC CHECKIDENT ('CandidateTags', RESEED, 0); Delete from ProfessionTags; DBCC CHECKIDENT ('CandidatePreferredRegions', RESEED, 0); Delete from Candidates;DBCC CHECKIDENT ('Candidates', RESEED, 0); ")
            .MyExecuteNonQuery();

            ADOAccess.GetDatabaseCommand()
            .SetSql("Delete from Zipcodes; DBCC CHECKIDENT ('Zipcodes', RESEED, 0); Delete from Cities;  DBCC CHECKIDENT ('Cities', RESEED, 0); Delete from Regions;  DBCC CHECKIDENT ('Regions', RESEED, 0); ")
            .MyExecuteNonQuery();



            //To create zipcodes table
            ADOAccess.GetDatabaseCommand()
                .SetSql("Select * from Old_region")
                .ExecuteReader(record =>
                {
                    object regionId;
                    int currentRegionId;
                    int currentCityId;
                    try
                    {
                        regionId = ADOAccess.GetDatabaseCommand()
                                    .SetSql(string.Format("Select  Id from Regions where RegionName='{0}'", record.SafeRead<string>("Region")))
                                    .MyExecuteScalar();
                        if (regionId == null)
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into Regions(RegionName) values({0})", InsertValue(record["Region"])))
                                .MyExecuteNonQuery(false);

                            regionId = ADOAccess.GetDatabaseCommand()
                                    .SetSql("select IDENT_CURRENT('Regions')")
                                    .MyExecuteScalar();

                        }
                        int.TryParse(regionId.ToString(), out currentRegionId);

                        string cityName = (string)record["CITY"];
                        if (string.IsNullOrEmpty(cityName))
                            cityName = "Other";

                        var city = ADOAccess.GetDatabaseCommand()
                            .SetSql(string.Format("Select  Id from Cities where City='{0}'", cityName))
                            .MyExecuteScalar();

                        if (city == null)
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into Cities(City,RegionId,ZipCode) values({0},{1},{2})", InsertValue(cityName), currentRegionId, 0))
                                .MyExecuteNonQuery(false);

                            city = ADOAccess.GetDatabaseCommand()
                                    .SetSql("select IDENT_CURRENT('Cities')")
                                    .MyExecuteScalar();

                        }
                        int.TryParse(city.ToString(), out currentCityId);


                        var zipcode = ADOAccess.GetDatabaseCommand()
                           .SetSql(string.Format("Select  Id from Zipcodes where Zipcode='{0}'", (string)record["Zipcode"]))
                           .MyExecuteScalar();

                        if (zipcode == null)
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into Zipcodes(Zipcode,CityId) values({0},{1})", InsertValue(record["Zipcode"]), currentCityId))
                                .MyExecuteNonQuery(false);


                        }




                    }
                    catch (Exception ex)
                    {
                        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                        logger.Fatal(ex);
                    }

                });




            // Update ProfessionTags table

            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values({0})", InsertValue("Faglig Dommer"))).MyExecuteNonQuery();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values({0})", InsertValue("Skønsmænd"))).MyExecuteNonQuery();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values({0})", InsertValue("Sagkyndig"))).MyExecuteNonQuery();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values({0})", InsertValue("Mediator"))).MyExecuteNonQuery();
            ADOAccess.GetDatabaseCommand()
                .SetSql(string.Format("Insert into ProfessionTags(Tag) values({0})", InsertValue("Enedommer"))).MyExecuteNonQuery();
            

            Dictionary<int, string> tags = new Dictionary<int, string>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select * from ProfessionTags")
                .ExecuteReader(record =>
                {
                    tags.Add(record.SafeRead<int>("Id"), record.SafeRead<string>("Tag"));
                });


            Dictionary<int, string> regionDictionary = new Dictionary<int, string>();
            ADOAccess.GetDatabaseCommand()
                .SetSql("select * from regions")
                .ExecuteReader(recordRegion =>
                {
                    regionDictionary.Add(recordRegion.SafeRead<int>("Id"), recordRegion.SafeRead<string>("RegionName"));
                });

            ADOAccess.GetDatabaseCommand()
                .SetSql(" Select  * from Contact_Info order by id")
                .ExecuteReader(record =>
                {
                    try
                    {
                        var zip = ADOAccess.GetDatabaseCommand()
                            .SetSql(string.Format("select Id from zipcodes where zipcode='{0}'", DBSafeRead<string>(record["Zipcode1"])))
                            .MyExecuteScalar();

                        if (zip == null)
                            zip = "NULL";
                        else
                            zip = "'" + zip + "'";

                        var ratings = ADOAccess.GetDatabaseCommand()
                        .SetSql(string.Format("select RATING from RATINGS where CID='{0}'", DBSafeRead<int>(record["ID"])))
                        .MyExecuteScalar();
                        if (ratings == null)
                            ratings = 0;

                        ADOAccess.GetDatabaseCommand()
                            // .SetSql(string.Format("Insert into Candidates(FirstName,LastName,Phonehome) values({0},{1},{2})", InsertValue(record["First_Name"]), InsertValue(record["Last_Name"]), InsertValue(record["Phone_Home"])))
                            .SetSql(string.Format("Insert into Candidates(FirstName,LastName,RegistrationId, PhoneHome,PhoneWork,PhoneMobile, Address,Notes, Company,Designation, Website,Status,LastUpdated,ZipcodeId,Email,Fax,Street,Stat_Provins,Ratings,Bygherre,Rådgiver,Entreprenør) " +
                                                                       " values({0},{1},{2} ,{3},{4},{5}, {6},{7}, {8},{9}, {10},{11},{12},{13}, {14},{15},{16},{17},{18},{19},{20},{21} )",
                                                                       InsertValue(record["First_Name"]),
                                                                       InsertValue(record["Last_Name"]),
                                                                       InsertValue(record["Old_Id"]),

                                                                       InsertValue(record["Phone_Home"]),
                                                                       InsertValue(record["Phone_Work"]),
                                                                       InsertValue(record["Mobile"]),

                                                                       InsertValue(record["Street"]),
                                                                       InsertValue(record["Note"]),

                                                                       InsertValue(record["Company"]),
                                                                       InsertValue(record["Position"]),


                                                                       InsertValue(record["Website"]),
                                                                       InsertValue(record["Status"]).ToString() == "Passiv" ? 0 : 1,

                                                                       InsertValue(DateTime.Now),
                                                                       zip,

                                                                       InsertValue(record["Email"]),
                                                                       InsertValue(record["fax"]),
                                                                       InsertValue(record["Street"]),
                                                                       InsertValue(record["Stat_Provins"]),
                                                                       ratings,
                                                                       InsertValue(record["Bygherre"]).ToString() == "'True'" ? 1 : 0,
                                                                       InsertValue(record["Rådgiver"]).ToString() == "'True'" ? 1 : 0,
                                                                       InsertValue(record["Entreprenør"]).ToString() == "'True'" ? 1 : 0

                                                                       ))

                            .MyExecuteNonQuery(false);

                        var insertedRecordId = ADOAccess.GetDatabaseCommand()
                            .SetSql("select IDENT_CURRENT('Candidates')")
                            .MyExecuteScalar();

                        if (record.SafeRead<string>("FDO") == "True")
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into CandidateTags(CandidateId,TagId) values({0},{1})", insertedRecordId, tags.FirstOrDefault(t => t.Value == "Faglig Dommer").Key))
                                .MyExecuteNonQuery();
                        }

                        if (record.SafeRead<string>("EMR") == "True")
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into CandidateTags(CandidateId,TagId) values({0},{1})", insertedRecordId, tags.FirstOrDefault(t => t.Value == "Skønsmænd").Key))
                                .MyExecuteNonQuery();
                        }


                        if (record.SafeRead<string>("Expert") == "True")
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into CandidateTags(CandidateId,TagId) values({0},{1})", insertedRecordId, tags.FirstOrDefault(t => t.Value == "Sagkyndig").Key))
                                .MyExecuteNonQuery();
                        }

                        if (record.SafeRead<string>("Mediator") == "True")
                        {
                            ADOAccess.GetDatabaseCommand()
                                .SetSql(string.Format("Insert into CandidateTags(CandidateId,TagId) values({0},{1})", insertedRecordId, tags.FirstOrDefault(t => t.Value == "Mediator").Key))
                                .MyExecuteNonQuery();
                        }


                        


                        var skill1 = record.SafeRead<string>("Class1");
                        var skill2 = record.SafeRead<string>("Class2");
                        var skill3 = record.SafeRead<string>("Class3");

                        if (!string.IsNullOrWhiteSpace(skill1))
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidateSkills(CandidateId,Skill) values({0},'{1}')", insertedRecordId, skill1))
                                            .MyExecuteNonQuery();
                        }

                        if (!string.IsNullOrWhiteSpace(skill2) && skill2 != skill1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidateSkills(CandidateId,Skill) values({0},'{1}')", insertedRecordId, skill2))
                                            .MyExecuteNonQuery();
                        }

                        if (!string.IsNullOrWhiteSpace(skill3) && skill3 != skill2 && skill3 != skill1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidateSkills(CandidateId,Skill) values({0},'{1}')", insertedRecordId, skill3))
                                            .MyExecuteNonQuery();
                        }


                        ADOAccess.GetDatabaseCommand()
                            .SetSql(string.Format("select * from Attachment where CID={0}", record.SafeRead<int>("Id")))
                            .ExecuteReader(attachRecord =>
                            {
                                ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidateFiles(CandidateId,FileContent,FileName) values({0},{1},{2})", insertedRecordId, InsertValue(attachRecord["FileMatter"]), InsertValue(attachRecord["FileName"])))
                                            .MyExecuteNonQuery();
                            });

                        var RGID1 = record.SafeRead<long>("RGID1");
                        var RGID2 = record.SafeRead<long>("RGID2");
                        var RGID3 = record.SafeRead<long>("RGID3");
                        var RGID4 = record.SafeRead<long>("RGID4");
                        var RGID5 = record.SafeRead<long>("RGID5");

                        if (RGID1 == 1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},'{1}')", insertedRecordId, regionDictionary.FirstOrDefault(i => i.Value == "Hovedstaden").Key))
                                            .MyExecuteNonQuery();
                        }
                        if (RGID2 == 1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},'{1}')", insertedRecordId, regionDictionary.FirstOrDefault(i => i.Value == "Sjælland").Key))
                                            .MyExecuteNonQuery();
                        }
                        if (RGID3 == 1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},'{1}')", insertedRecordId, regionDictionary.FirstOrDefault(i => i.Value == "Syddanmark").Key))
                                            .MyExecuteNonQuery();
                        }
                        if (RGID4 == 1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},'{1}')", insertedRecordId, regionDictionary.FirstOrDefault(i => i.Value == "Midtjylland").Key))
                                            .MyExecuteNonQuery();
                        }
                        if (RGID5 == 1)
                        {
                            ADOAccess.GetDatabaseCommand()
                                            .SetSql(string.Format("Insert into CandidatePreferredRegions(CandidateId,RegionId) values({0},'{1}')", insertedRecordId, regionDictionary.FirstOrDefault(i => i.Value == "Nordjylland").Key))
                                            .MyExecuteNonQuery();
                        }


                        //copy attachments
                        // move this
                        string userFiles = @"C:\CVWebsitePrimary\CVWebsite\Files\UserSpecific\" + DBSafeRead<int>(record["ID"]);

                        if (System.IO.Directory.Exists(userFiles))
                        {
                            var targetPath = System.IO.Path.Combine(attachmentsFolderOnServer, insertedRecordId.ToString());

                            if (System.IO.Directory.Exists(targetPath))
                                System.IO.Directory.Delete(targetPath);

                            System.IO.Directory.CreateDirectory(targetPath);

                            string[] files = System.IO.Directory.GetFiles(userFiles);

                            // Copy the files and overwrite destination files if they already exist.
                            foreach (string s in files)
                            {
                                // Use static Path methods to extract only the file name from the path.
                                var fileName = System.IO.Path.GetFileName(s);
                                var destFile = System.IO.Path.Combine(targetPath, fileName);
                                System.IO.File.Copy(s, destFile, true);
                            }
                        }



                    }
                    catch (Exception ex)
                    {
                        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                        logger.Fatal(ex);
                    }
                });

          


            //document group

            ADOAccess.GetDatabaseCommand()
                                .SetSql("Truncate Table DocumentGroups; Truncate table Documents; select * from old_documents ")
                                .ExecuteReader(record =>
                                {
                                    object level1 = 0, level2 = 0, level3 = 0;
                                    int groupid=0;
                                    var guid = Guid.NewGuid();

                                    level1 = ADOAccess.GetDatabaseCommand()
                                    .SetSql(string.Format("Select  Id from DocumentGroups where Name='{0}'", (string)record["Group_Nm"]))
                                    .MyExecuteScalar();
                                            if (level1 == null)
                                            {
                                                ADOAccess.GetDatabaseCommand()
                                                    .SetSql(string.Format("Insert into DocumentGroups(Name) values({0})", InsertValue(record["Group_Nm"])))
                                                    .MyExecuteNonQuery(false);

                                                level1 = ADOAccess.GetDatabaseCommand()
                                                        .SetSql("select IDENT_CURRENT('DocumentGroups')")
                                                        .MyExecuteScalar();
                                                
                                            }
                                            int.TryParse(level1.ToString(), out groupid);

                                    //level2
                                            if (!string.IsNullOrWhiteSpace(record["SubGroup_Nm"].ToString()))
                                            {
                                                level2 = ADOAccess.GetDatabaseCommand()
                                               .SetSql(string.Format("Select  Id from DocumentGroups where Name='{0}'", (string)record["SubGroup_Nm"]))
                                               .MyExecuteScalar();
                                                if (level2 == null)
                                                {
                                                    ADOAccess.GetDatabaseCommand()
                                                        .SetSql(string.Format("Insert into DocumentGroups(Name,ParentGroupId) values({0},{1})", InsertValue(record["SubGroup_Nm"]), level1.ToString()))
                                                        .MyExecuteNonQuery(false);

                                                    level2 = ADOAccess.GetDatabaseCommand()
                                                            .SetSql("select IDENT_CURRENT('DocumentGroups')")
                                                            .MyExecuteScalar();
                                                    

                                                }
                                                int.TryParse(level2.ToString(), out groupid);
                                            }

                                    //level3
                                            if (!string.IsNullOrWhiteSpace(record["SGroup"].ToString()))
                                            {
                                                level3 = ADOAccess.GetDatabaseCommand()
                                                       .SetSql(string.Format("Select  Id from DocumentGroups where Name='{0}'", (string)record["SGroup"]))
                                                       .MyExecuteScalar();
                                                if (level3 == null)
                                                {
                                                    ADOAccess.GetDatabaseCommand()
                                                        .SetSql(string.Format("Insert into DocumentGroups(Name,ParentGroupId) values({0},{1})", InsertValue(record["SGroup"]), level2.ToString()))
                                                        .MyExecuteNonQuery(false);

                                                    level3 = ADOAccess.GetDatabaseCommand()
                                                            .SetSql("select IDENT_CURRENT('DocumentGroups')")
                                                            .MyExecuteScalar();

                                                    

                                                }
                                                int.TryParse(level3.ToString(), out groupid);
                                            }

                                            ADOAccess.GetDatabaseCommand()
                                                            .SetSql(string.Format("Insert into Documents(FileName,DocumentGroupId,Created,Tags,FileContent,ServerFileName) values({0},{1},'{2}',{3},{4},{5})",
                                                              InsertValue(record["File_Name"]),
                                                              groupid,
                                                              DateTime.Now.Date,
                                                              InsertValue(record["Metatag_Nm"]),
                                                              InsertValue(record["F_Matter"]),
                                                              InsertValue(string.Format("{0}{1}", guid.ToString(), System.IO.Path.GetExtension(record["F_Path"].ToString())))
                                                              ))
                                                            .MyExecuteNonQuery(false);

                                         //copy file
                                            var sourcepath = record["F_Path"].ToString();
                                            if (System.IO.File.Exists(sourcepath))
                                            {
                                                var targetPath = System.IO.Path.Combine(documentsFolderPathOnServer, string.Format("{0}{1}", guid.ToString(),System.IO.Path.GetExtension(sourcepath)));
                                                System.IO.File.Copy(sourcepath, targetPath, true);
                                            }
                                             

                                });


           







            //


            


            





            return Content("Done");
        }

        // GET: CV/Details/5
        public ActionResult Details(int id)
        {
            string CV = System.Configuration.ConfigurationManager.AppSettings.Get("CVExternalPage");
            var candidatedetails = _candidateService.GetCandidateDetails(id);
            candidatedetails.CUrl = CV;

            if (Request.IsAjaxRequest())
                return PartialView(candidatedetails);
            else
                return View(candidatedetails);
        }

        // GET: CV/Create
        public ActionResult Create()
        {
            Candidate candidate = new Candidate { Status = CandidateStatus.Active };
            return View(candidate);
        }

        // POST: CV/Create
        [HttpPost]
        public ActionResult Create(Candidate candidate)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _candidateService.AddNew(candidate);
                    return RedirectToAction("Index", new { Id = candidate.Id });
                }

                return View(candidate);

            }
             catch(Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View(candidate);
            }
        }

        // GET: CV/Edit/5
        public ActionResult Edit(int id)
        {
            var candidatedetails = _candidateService.GetCandidate(id);
            return PartialView(candidatedetails);
        }

        // POST: CV/Edit/5
        [HttpPost]
        public ActionResult Edit(Candidate candidate)
        {
            try
            {

                if(ModelState.IsValid)
                {
                    _candidateService.Update(candidate);
                    return Content( candidate.Id.ToString() );
                }

                this.Response.StatusCode = 400;
                return PartialView(candidate);
            }
            catch(Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(candidate);
            }
        }

        // GET: CV/Delete/5
        public ActionResult Delete(int id)
        {
            var candidatedetails = _candidateService.GetCandidate(id);
            return PartialView(candidatedetails);
        }


        // POST: CV/Delete/5
        [HttpPost]
        public ActionResult Delete(Candidate candidate, FormCollection collection)
        {
            try
            {
              

                _candidateService.Delete(candidate);

                string path = @"~\AttachedFiles\" + candidate.Id.ToString();
                string accpath = Server.MapPath(path);
                if (Directory.Exists(accpath))
                {
                    Directory.Delete(accpath, true);
                }

                return Content(candidate.Id.ToString());
            }
            catch(Exception ex)
            {
                 this.Response.StatusCode = 400;
                 ModelState.AddModelError("", ex.Message);
                 return PartialView(candidate);

            }
        }

        //22.11.16.....Appa...To copy file path into DB table COPYPATH..through web service...
          public ActionResult Copypath(int id, string file)
        {
          
            if (file != "")
            {
                string pcname = Environment.MachineName;
                string filenames = "\\\\" + pcname + "\\CVApp2\\AttachedFiles\\" + id + "\\" + file + "";
             
             

                FileInfo Fname = new FileInfo(filenames);
                if (Fname.Exists)
                {
                    string clientMachineName = (System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);
                  

                    Clip.MyService myservice = new Clip.MyService();

                  
                    string NewFileName = myservice.SendPath(filenames, id, clientMachineName);
                    return Json(new { NewFileName = NewFileName });

                   
                }
                else
                {
                    return Json(new { NewFileName = "" });
                }
            }
            else
            {
                return Json(new { NewFileName = "" });
            }
        }

         [OutputCache(NoStore=true,Duration=0)]
          public ActionResult ShowAttachment(int id,string filename)
          {
              filename = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(filename));

              var candidatedetails = _candidateService.GetCandidate(id);

              ViewBag.Title = candidatedetails.FirstName + " " + candidatedetails.LastName;
              ViewBag.FileName =  string.Format(@"/AttachedFiles/{0}/{1}", id, filename);
            
              return View();

          }
         
     
        public ActionResult CustomersFromSharepoint(string term)
          {

              var customers = _candidateService.CustomersFromSharepoint(term);

              return Json(customers,JsonRequestBehavior.AllowGet);
          }


        public ActionResult CustomerFromSharepoint(string registrationNumber)
        {
            var candidate = _candidateService.CustomerFromSharepoint(registrationNumber);

            return Json(candidate, JsonRequestBehavior.AllowGet);
        }

    }
}
