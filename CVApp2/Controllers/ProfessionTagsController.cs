﻿using CVApp2.Data;
using CVApp2.Models;
using CVApp2.Services;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVApp2.Controllers
{
    public class ProfessionTagsController : Controller
    {
        // GET: ProfessionTags

        IProfessionTagService _professionTagsService;

        public ProfessionTagsController()
        {
            _professionTagsService = new ProfessionTagService();
        }
        public ActionResult Index()
        {
            var professionTag = _professionTagsService.GetAllTags();

            return View(professionTag);
        }

        // GET: ProfessionTags/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProfessionTags/Create
        public ActionResult Create()
        {
            ProfessionTag professionTag = new ProfessionTag();

            return PartialView("AddEditPartialTag", professionTag);
        }

        // POST: ProfessionTags/Create
        [HttpPost]
        public ActionResult Create(ProfessionTag professionTag)
        {
             try
            {
                if (ModelState.IsValid)
                {
                    _professionTagsService.AddNew(professionTag);

                    return RedirectToAction("Index");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");
                return PartialView("AddEditPartialTag", professionTag);

              
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartialTag", professionTag);
            }
        }

        // GET: ProfessionTags/Edit/5
        public ActionResult Edit(int id)
        {
            ProfessionTag professionTag = _professionTagsService.GetProfessionTagByID(id);

            return PartialView("AddEditPartialTag", professionTag);
        }

        // POST: ProfessionTags/Edit/5
        [HttpPost]
        public ActionResult Edit(ProfessionTag professionTag)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _professionTagsService.Update(professionTag);

                    return RedirectToAction("Index");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("AddEditPartialTag", professionTag);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "korrigere venligst følgende");

                return PartialView("AddEditPartialTag", professionTag);
            }
        }

        // GET: ProfessionTags/Delete/5
        public ActionResult Delete(int id)
        { 
            var professionTag= _professionTagsService.GetProfessionTagByID(id);

            return PartialView(professionTag);
        }

        // POST: ProfessionTags/Delete/5
        [HttpPost]
        public ActionResult Delete(ProfessionTag professionTag, FormCollection collection)
        {
            try
            {
                _professionTagsService.Delete(professionTag);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(professionTag);
            }
        }
    }
}
