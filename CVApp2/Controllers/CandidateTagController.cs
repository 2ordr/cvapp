﻿using CVApp2.Models;
using CVApp2.Services;
using CVApp2.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{
    public class CandidateTagController : Controller
    {
        ICandidateTagService _candidateTagService;
        IProfessionTagService _professionTagService;

        public CandidateTagController()
        {
            _candidateTagService = new CandidateTagService();
            _professionTagService = new ProfessionTagService();
        }


        // GET: CandidateTag
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None)] 
        public ActionResult CandidateTags(int candidateId)
        {

            var allTags = _professionTagService.GetAll();

            var candidateTags = _candidateTagService.GetCandidateTags(candidateId).Select(x => x.TagId);


            List<CandidateTagVM> result  = new List<CandidateTagVM>();

            foreach(var item in allTags)
            {
                CandidateTagVM tag =  new CandidateTagVM { IsSelected=false, CandidateId=candidateId, ProfessionTag=item , TagId=item.Id};

                if (candidateTags.Contains(item.Id))
                    tag.IsSelected = true;

                result.Add(tag);
            }


            ViewBag.CandidateId = candidateId;
            return PartialView("Index", result);
        }


        // GET: CandidateTag/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        public ActionResult AddDeleteTag(int candidateId,int tagId,bool add)
        {
            CandidateTag candidateTag = new CandidateTag { CandidateId=candidateId,TagId=tagId};

            if (add)
                _candidateTagService.AddNew(candidateTag);
            else
                _candidateTagService.Delete(candidateTag);

            return RedirectToAction("CandidateTags", new { candidateId = candidateTag.CandidateId });
        }


        // GET: CandidateTag/Create
        public ActionResult Create(int candidateId)
        {

            dynamic model = new ExpandoObject();


            List<Tuple<int, string, bool, bool>> tags = new List<Tuple<int, string, bool, bool>>();

            tags.Add( Tuple.Create(1, "one", false, false));
            tags.Add(Tuple.Create(2, "two", true, true));


            

            model.List = tags;

            model.CandidateId = candidateId;

            model.Id = 1;



            CandidateTag candidateTag = new CandidateTag();
            candidateTag.CandidateId = candidateId;
            ViewBag.TagList = _professionTagService.GetAll().ToList();
            return PartialView("AddEditCandidateTagPartial", model);
        }

        // POST: CandidateTag/Create
        [HttpPost]
        public ActionResult Create(dynamic candidateTag)
        {
            try
            {
                _candidateTagService.AddNew(candidateTag);

                return RedirectToAction("CandidateTags", new { candidateId = candidateTag.CandidateId });
            }
            catch(Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.TagList = _professionTagService.GetAll().ToList();
                return PartialView("AddEditCandidateTagPartial", candidateTag);
            }
        }

        // GET: CandidateTag/Edit/5
        public ActionResult Edit(int id)
        {
            CandidateTag candidateTag = _candidateTagService.GetCandidateById(id);
            ViewBag.TagList = _professionTagService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Tag }).ToList();

            return PartialView("AddEditCandidateTagPartial", candidateTag);
        }

        // POST: CandidateTag/Edit/5
        [HttpPost]
        public ActionResult Edit(CandidateTag candidateTag)
        {
            try
            {
                _candidateTagService.Update(candidateTag);

                return RedirectToAction("CandidateTags", new { candidateId = candidateTag.CandidateId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.TagList = _professionTagService.GetAll().ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Tag }).ToList();
                return PartialView("AddEditCandidateTagPartial", candidateTag);
            }
        }

        // GET: CandidateTag/Delete/5
        public ActionResult Delete(int id)
        {
            var candidateTag = _candidateTagService.GetCandidateById(id);
            _candidateTagService.Delete(candidateTag);
            return RedirectToAction("CandidateTags", new { candidateId = candidateTag.CandidateId });

        }

        // POST: CandidateTag/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
