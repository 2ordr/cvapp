﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVApp2.Controllers
{
    public class KnowledgeController : Controller
    {
        IDocumentGroupService _documentGroupService;
        IDocumentService _documentService;

        public KnowledgeController()
        {
            _documentService = new DocumentService();
            _documentGroupService = new DocumentGroupService();
        }


        // GET: Knowledge
        public ActionResult Index(int? selectedGroup)
        {
            ViewBag.SelectedGroup = selectedGroup;
            return View();
        }

        // GET: Knowledge/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Knowledge/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Knowledge/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {

                return View();
            }
        }

        // GET: Knowledge/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Knowledge/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Knowledge/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Knowledge/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ShowAttachment(int id)
        {
            var documentdetails = _documentService.GetDocumentById(id);
            var documentGroup = _documentGroupService.GetDocumentGroupById(documentdetails.DocumentGroupId);

            documentdetails.DocumentGroup = documentGroup;

            //var fileName = documentdetails.ServerFileName;

            //ViewBag.Title = documentdetails.FileName;


            //ViewBag.DocumentGroupName = DocumentGroupdetails.Name;
            //ViewBag.Created = documentdetails.Created;
            //ViewBag.Tags = documentdetails.Tags;

            //ViewBag.JNo = documentdetails.JNo;
            //ViewBag.JNoDate = documentdetails.JDate;
            //ViewBag.ReleasedForPublication = documentdetails.ReleasedForPublication;
            //ViewBag.KarnovNo = documentdetails.KarnovNo;
            //ViewBag.KarnovDate = documentdetails.KarnovDate;
            //ViewBag.TBBNo = documentdetails.TBBNo;
            //ViewBag.TBBDate = documentdetails.TBBDate;
            //ViewBag.Summary = documentdetails.Summary;
            //ViewBag.ArbitrationCourtComposition = documentdetails.ArbitrationCourtComposition;

            ViewBag.FileName = string.Format(@"/DocumentFiles/{0}", documentdetails.ServerFileName);

            return View(documentdetails);

        }
    }
}
