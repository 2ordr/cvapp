﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{
    public class CityController : Controller
    {
          ICityService _cityService;
          IRegionService _regionService;
          IZipCodeService _ZipCodeService;
        public CityController()
        {
            _cityService = new CityService();
            _regionService = new RegionService();
            _ZipCodeService = new ZipCodeService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None)] 
        public ActionResult CityDetails(int id)
        {
            var Cities = _regionService.GetCityDetailsByID(id);
            return View("Index");

        }
        // GET: City/Details/5
        public ActionResult Details(int id)
        {
            var Zipcodes = _cityService.GetZipCodeDetailsByCity(id);
            //ViewBag.CityName = _cityService.GetCityByID(id);
           
            City city = new City();
            city = _cityService.GetCityByID(id);

            ViewBag.City = city;

            return View(Zipcodes);
        }

        // GET: City/Create
        public ActionResult Create(int regionId)
        {
            City city = new City { RegionId = regionId }; 

            return PartialView("AddEditPartial", city);
        }

        // POST: City/Create
        [HttpPost]
        public ActionResult Create(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cityService.AddNew(city);

                    return Content("Success");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Ret venligst følgende fejl:");

                return PartialView("AddEditPartial", city);


            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
               
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", city);
            }
        }

        // GET: City/Edit/5
        public ActionResult Edit(int id)
        {
            City city = _cityService.GetCityByID(id);

            return PartialView("AddEditPartial", city);
        }

        // POST: City/Edit/5
        [HttpPost]
        public ActionResult Edit(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cityService.Update(city);

                    return RedirectToAction("Details", "Region", new { id = city.RegionId });
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("AddEditPartial", city);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "korrigere venligst følgende");

                return PartialView("AddEditPartial", city);
            }
         
        }

        // GET: City/Delete/5
        public ActionResult Delete(int id)
        {
            var city = _cityService.GetCityByID(id);

            return PartialView(city);
        }

        // POST: City/Delete/5
        [HttpPost]
        public ActionResult Delete(City city, FormCollection collection)
        {
            try
            {
                _cityService.Delete(city);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(city);
            }
        }
    }
}
