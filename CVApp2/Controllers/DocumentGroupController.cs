﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{
    public class DocumentGroupController : Controller
    {

         IDocumentGroupService _documentGroupService;
         IDocumentService _documentService;
         public DocumentGroupController()
        {
            _documentGroupService = new DocumentGroupService();
        }

        public ActionResult GroupTree()
        {
            var documentGroups = _documentGroupService.GetAll();
            return PartialView(documentGroups);
        }

        // GET: DocumentGroup
        public ActionResult Index()
        {
            var documentGroups = _documentGroupService.GetAll();
            return View(documentGroups);
        }

        // GET: DocumentGroup/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DocumentGroup/Create
        public ActionResult Create()
        {
            DocumentGroup documentGroup = new DocumentGroup();
            return View(documentGroup);
        }

        // POST: DocumentGroup/Create
        [HttpPost]
        public ActionResult Create(DocumentGroup documentGroup)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    _documentGroupService.AddNew(documentGroup);
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(documentGroup);
                }

            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(documentGroup);
            }
        }

        // GET: DocumentGroup/Edit/5
        public ActionResult Edit(int id)
        {
            var documentGroup = _documentGroupService.GetDocumentGroupById(id);
            return View(documentGroup);
        }

        // POST: DocumentGroup/Edit/5
        [HttpPost]
        public ActionResult Edit(DocumentGroup documentGroup, FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _documentGroupService.Update(documentGroup);
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(documentGroup);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(documentGroup);
            }
        }

        // GET: DocumentGroup/Delete/5

        public ActionResult Delete(int id)
        {
            DocumentGroup documentGroup = _documentGroupService.GetDocumentGroupById(id);
           
            return PartialView(documentGroup);
        }

        // POST: DocumentGroup/Delete/5
        [HttpPost]
        public ActionResult Delete(DocumentGroup documentGroup)
        {
            try
            {
                _documentGroupService.Delete(documentGroup);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(documentGroup);
            }
        }
    }
}
