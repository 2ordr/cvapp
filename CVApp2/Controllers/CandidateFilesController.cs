﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Text;
using System.Web.UI;
using CVApp2.Data;

namespace CVApp2.Controllers
{
    public class CandidateFilesController : Controller
    {
        private const string PICTURE_PATH = "/AttachedFiles/";

        ICandidateFileService _candidateFileService;

        public CandidateFilesController()
        {
            _candidateFileService = new CandidateFileService();
        }

        // GET: CandidateFiles
        public ActionResult Index()
        {
            return View();
        }

       [OutputCache(Location = OutputCacheLocation.None)] 
        public ActionResult CandidateFiles(int candidateId)
        {
            var candidateSkills = _candidateFileService.GetCandidateFiles(candidateId);
            ViewBag.CandidateId = candidateId;
            return PartialView("Index", candidateSkills);
        }

        // GET: CandidateFiles/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CandidateFiles/Create
        public ActionResult Create(int candidateId)
        {
            CandidateFile candidateFile = new CandidateFile();
            candidateFile.CandidateId = candidateId;
            return PartialView("AddEditPartial", candidateFile);
        }

        // POST: CandidateFiles/Create
        [HttpPost]
        public ActionResult Create(int candidateId, FormCollection collection)
        {
            try
            {
                
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    
                    var uploadedFile = Request.Files[0];
                    var uploadFileName =Path.GetFileName(uploadedFile.FileName);

                    
                    
                    string localpath = Path.Combine(PICTURE_PATH, candidateId.ToString());
                    localpath = HostingEnvironment.MapPath(localpath);

                    if (!Directory.Exists(localpath))
                        Directory.CreateDirectory(localpath);

                  
                    localpath = Path.Combine(localpath, uploadFileName);                   

                    uploadedFile.SaveAs(localpath);
                  
                    string FileContent = ReadFileData(localpath);

                    _candidateFileService.AddNew(new CandidateFile { CandidateId = candidateId, FileName = uploadFileName, FileContent = FileContent });
                }


                return RedirectToAction("CandidateFiles", "CandidateFiles", new { candidateId = candidateId });
            }
            catch(Exception ex)
            {
                Response.StatusCode = 400;
                return Content("Error");
            }
        }




        public static string ReadFileData(string path)
        {
            string Readeddata = "";

            if (path.Contains(".txt"))
            {
                string[] lines = System.IO.File.ReadAllLines(path);

                Readeddata = string.Join(" ", lines);
            }

            if (path.Contains(".pdf") || path.Contains(".PDF"))
            {
                string datacheck = parsePDF(path);
                Readeddata = datacheck;

            }

            if (path.Contains(".docx") || path.Contains(".doc"))
            {
                string datacheck = ReadMsWord(path);
                Readeddata = datacheck;
            }

            if (path.Contains(".html") || path.Contains(".htm"))
            {
                System.Text.StringBuilder htmlContent = new System.Text.StringBuilder();

                htmlContent = ReadHtmlFile(path);

                Readeddata = Convert.ToString(htmlContent);

            }
            if (path.Contains(".xlsx") || path.Contains(".xls"))
            {
              
                Readeddata = "";
            }


            return Readeddata;
        }


        public static string parsePDF(string pdf_in)
        {

            using (iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(pdf_in))
            {
                StringBuilder text = new StringBuilder();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(reader, i));
                }

                return text.ToString();
            }

        }

        public static string ReadMsWord(string filepathd)
        {
            // variable to store file path
            string filePath = filepathd;


            try
            {
                Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
                object miss = System.Reflection.Missing.Value;
                object path = filePath;
                object readOnly = true;
                Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
                string totaltext = "";
                for (int i = 0; i < docs.Paragraphs.Count; i++)
                {
                    totaltext += " \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString();
                }
                return totaltext;
            }
            catch (Exception ex)
            {
                return "";

            }

        }

        public static System.Text.StringBuilder ReadHtmlFile(string htmlFileNameWithPath)
        {
            System.Text.StringBuilder htmlContent = new System.Text.StringBuilder();
            string line;
            try
            {
                using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(htmlFileNameWithPath))
                {

                    while ((line = htmlReader.ReadLine()) != null)
                    {
                        htmlContent.Append(line);
                    }
                }
            }
            catch (Exception objError)
            {
                throw objError;
            }

            return htmlContent;
        }

        // GET: CandidateFiles/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CandidateFiles/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CandidateFiles/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                var candidateFile = _candidateFileService.GetCandidateFilesbyID(id);
                _candidateFileService.Delete(candidateFile);

                string fname = candidateFile.FileName;
                string localpath = Path.Combine(PICTURE_PATH, candidateFile.CandidateId.ToString());


                string fullPath = Request.MapPath("~" + localpath + "/" + fname);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                return RedirectToAction("CandidateFiles", "CandidateFiles", new { candidateId = candidateFile.CandidateId });
            }

            catch
            {
                Response.StatusCode = 400;
                return Content("Error");
            }
        }

        // POST: CandidateFiles/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult ListMissingFiles()
        {

            List<string> result = new List<string>();

            var files = _candidateFileService.GetAll();


            foreach(var file in files)
            {
                string fname = file.FileName;
                string localpath = Path.Combine(PICTURE_PATH, file.CandidateId.ToString());


                string fullPath = Request.MapPath("~" + localpath + "/" + fname);

                if(!System.IO.File.Exists(fullPath))
                {
                    result.Add(fullPath);
                }
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Files which are physically present, but not in DB
        /// </summary>
        /// <returns></returns>
        public ActionResult ListNotTrackedFiles()
        {

            List<string> result = new List<string>();

            var path = Request.MapPath(PICTURE_PATH);

            foreach(var file in Directory.EnumerateFiles(path,"*.*",SearchOption.AllDirectories))
            {
                string filename = Path.GetFileName(file);

                var exists = ADOAccess.GetDatabaseCommand()
                                    .SetSql(string.Format("Select  Id from candidateFiles where filename='{0}'", filename))
                                    .MyExecuteScalar();

                if(exists==null)
                    result.Add(file);
            }

            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}
