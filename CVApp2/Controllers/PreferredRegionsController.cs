﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{

    public class PreferredRegionsController : Controller
    {
        IPreferredRegionService _preferredRegionService;
        IRegionService _regionService;

        public PreferredRegionsController()
        {
            _preferredRegionService = new PreferredRegionService();

            _regionService = new RegionService();
        }

        // GET: PreferredRegions

        public ActionResult Index()
        {
            var preferredRegions = _preferredRegionService.GetAll();
            return View(preferredRegions);
        }


        [OutputCache(Location=OutputCacheLocation.None)] 
        public ActionResult CandidateRegions(int candidateId)
        {
            var candidateRegions = _preferredRegionService.GetCandidateRegions(candidateId);
            ViewBag.CandidateId = candidateId;
            return PartialView("Index", candidateRegions);
        }

        // GET: PreferredRegions/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PreferredRegions/Create
        public ActionResult Create(int candidateId)
        {
            PreferredRegion preferredRegion = new PreferredRegion();
            preferredRegion.CandidateId = candidateId;
            ViewBag.RegionList = _regionService.GetFiletered("select * from Regions").ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.RegionName }).ToList();
            return PartialView("AddEditPartial", preferredRegion);
        }

        // POST: PreferredRegions/Create
        [HttpPost]
        public ActionResult Create(PreferredRegion preferredRegion)
        {
            try
            {
                _preferredRegionService.AddNew(preferredRegion);

                return RedirectToAction("CandidateRegions", new { candidateId = preferredRegion.CandidateId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.RegionList = _regionService.GetFiletered("select * from Regions").ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.RegionName }).ToList();
                return PartialView("AddEditPartial", preferredRegion);
            }
        }

        // GET: PreferredRegions/Edit/5
        public ActionResult Edit(int id)
        {
            PreferredRegion preferredRegion = _preferredRegionService.GetPreferredRegion(id);
            ViewBag.RegionList = _regionService.GetFiletered("select * from Regions").ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.RegionName }).ToList();
            
            
            return PartialView("AddEditPartial",preferredRegion);
        }

        // POST: PreferredRegions/Edit/5
        [HttpPost]
        public ActionResult Edit(PreferredRegion preferredRegion)
        {
            try
            {
                _preferredRegionService.Update(preferredRegion);

                return RedirectToAction("CandidateRegions", new { candidateId = preferredRegion.CandidateId });
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("", ex.Message);

                ViewBag.RegionList = _regionService.GetFiletered("select * from Regions").ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.RegionName }).ToList();
                return PartialView("AddEditPartial", preferredRegion);
            }

        }


        // GET: PreferredRegions/Delete/5

        public ActionResult Delete(int id)
        {
            var preferredRegion = _preferredRegionService.GetPreferredRegion(id);
            _preferredRegionService.Delete(preferredRegion);
            return RedirectToAction("CandidateRegions", new { candidateId = preferredRegion.CandidateId });
        }

        //// POST: PreferredRegions/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
                

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
