﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace CVApp2.Controllers
{
    public class RegionController : Controller
    {
        IRegionService _regionService;

        public RegionController()
        {
            _regionService = new RegionService();
        }



        public ActionResult ZipCodeDetails(string zipcodeStr)
        {
            var zipcode = _regionService.GetZipDetailsFromZipcode(zipcodeStr);
            return PartialView(zipcode);
        }

        // GET: Region
        public ActionResult Index()
        {
            var Regions = _regionService.GetAll();

            return View(Regions);
        }

        // GET: Region/Details/5
        public ActionResult Details(int id, string currentFilter, string searchString, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var Cities = _regionService.GetCityDetailsByID(id);
           
            Region regions = _regionService.GetRegionsByID(id);
            Session["RegionName"] = regions.RegionName;
            Session["regionid"] = regions.Id;

            ViewBag.RegionId = id;

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(Cities.ToPagedList(pageNumber,pageSize));
        }

        // GET: Region/Create
        public ActionResult Create()
        {
            Region regions = new Region();

            return PartialView("AddEditPartial", regions);
        }

        // POST: Region/Create
        [HttpPost]
        public ActionResult Create(Region region)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _regionService.AddNew(region);

                    return Content("Success");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");
                return PartialView("AddEditPartial", region);

              
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", region);
            }
        }

        // GET: Region/Edit/5
        public ActionResult Edit(int id)
        {

            Region regions = _regionService.GetRegionsByID(id);

            return PartialView("AddEditPartial", regions);
           
        }

        // POST: Region/Edit/5
        [HttpPost]
        public ActionResult Edit(Region region)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _regionService.Update(region);

                    return Content("Success");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("AddEditPartial", region);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView("AddEditPartial", region);
            }
        }

        // GET: Region/Delete/5
        public ActionResult Delete(int id)
        {
            var region = _regionService.GetRegionsByID(id);
            return PartialView(region);
        }

        // POST: Region/Delete/5
        [HttpPost]
        public ActionResult Delete(Region region, FormCollection collection)
        {
            try
            {
                _regionService.Delete(region);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(region);
            }
        }

       
    }
}
