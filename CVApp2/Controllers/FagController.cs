﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace CVApp2.Controllers
{
    public class FagController : Controller
    {
        IFagService _FagService;

        public FagController()
        {
            _FagService = new FagService();
        }
        public ActionResult Index(string searchString, int? page)        {
            

            ViewBag.CurrentFilter = searchString;
         
            var Fag = _FagService.SearchFag(searchString);

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(Fag.ToPagedList(pageNumber,pageSize));
        }

    
        // GET: Fag/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Fag/Create
        public ActionResult Create()
        {
            Fag fag = new Fag();

            return PartialView("AddEditPartial", fag); 
           
        }

        // POST: Fag/Create
        [HttpPost]
        public ActionResult Create(Fag model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string fag = model.fag.Trim();
                    fag = fag.First().ToString().ToUpper() + fag.Substring(1);
                    model.fag = fag;
                    _FagService.AddNew(model);

                    return RedirectToAction("Index");
                }

                this.Response.StatusCode = 400;

                return PartialView("AddEditPartial", model);


            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", model);
            }
        }

        // GET: Fag/Edit/5
        public ActionResult Edit(int id)
        {
            Fag fag = _FagService.GetFagByID(id);

            return PartialView("AddEditPartial", fag);

        }

        // POST: Fag/Edit/5
        [HttpPost]
        public ActionResult Edit(Fag model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                     string fag = model.fag.Trim();                   
                     fag = fag.First().ToString().ToUpper() + fag.Substring(1);                
                     model.fag = fag;
                    _FagService.Update(model);

                    return RedirectToAction("Index");
                }

                this.Response.StatusCode = 400;

                return PartialView("AddEditPartial", model);


            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", model);
            }
        }

        // GET: Fag/Delete/5
        public ActionResult Delete(int id)
        {
            var Fag = _FagService.GetFagByID(id);

            return PartialView(Fag);
        }

        // POST: Fag/Delete/5
        [HttpPost]
        public ActionResult Delete(Fag model, FormCollection collection)
        {
            try
            {
                _FagService.Delete(model);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(model);
            }
        }
    }
}
