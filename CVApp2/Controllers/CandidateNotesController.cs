﻿using CVApp2.Models;
using CVApp2.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{
    public class CandidateNotesController : Controller
    {
          Logger logger = LogManager.GetCurrentClassLogger(); 
          ICandidateNotesService _candidateNotesService;

          public CandidateNotesController()
        {
            _candidateNotesService = new CandidateNotesService();
        }

          public ActionResult CandidateNotes(int candidateId)
          {
              var candidateNotes = _candidateNotesService.GetCandidateNotes(candidateId);
              return Json(new { CandidateId = candidateId, Notes = candidateNotes },JsonRequestBehavior.AllowGet);
          }

        // GET: CandidateNotes
          public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(int id)
        {
            CandidateNotes candidateNotes = new CandidateNotes();
            ViewBag.CandidateId = id;
            candidateNotes.CandidateId = id;
            return PartialView("Create", candidateNotes);

        }

       
        [HttpPost]
        public ActionResult Create(CandidateNotes model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    model.Date = System.DateTime.Now;
                    model.UserName =  User.Identity.Name;
                    _candidateNotesService.AddNew(model);
                    Response.CacheControl = "no-cache";
                    return RedirectToAction("CandidateNotes", "CandidateNotes" , new { candidateId = model.CandidateId }); 
                }

                this.Response.StatusCode = 400;
                Response.CacheControl = "no-cache";

                string err = "Validation Error";
                return Content(err);


            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return Content(ex.Message + ex.StackTrace);
            }
        }

        public ActionResult Delete(int id)
        {
            var candidateNote = _candidateNotesService.GetCandidateNotesById(id);
            _candidateNotesService.Delete(candidateNote);
         
            return RedirectToAction("CandidateNotes", new { candidateId = candidateNote.CandidateId });
        
        }

    
     
       

    }
}