﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CVApp2.Controllers
{
    public class CandidateSkillController : Controller
    {
        ICandidateSkillService _candidateSkillService;
        ICandidateService _candidateService;
        IFagService _fagService;
        public CandidateSkillController()
        {
            _candidateSkillService = new CandidateSkillService();
            _candidateService = new CandidateService();
            _fagService = new FagService();
        }

        // GET: CandidateSkill
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None)] 
        public ActionResult CandidateSkills(int candidateId)
        {
            var candidateSkills = _candidateSkillService.GetCandidateSkills(candidateId);
            ViewBag.CandidateId = candidateId;
            return PartialView("Index", candidateSkills);
        }


        // GET: CandidateSkill/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CandidateSkill/Create
        public ActionResult Create(int candidateId)
        {

            ViewBag.SkillsGrouped = GetSkillGroups(); 

            CandidateSkill candidateSkill = new CandidateSkill();
            candidateSkill.CandidateId = candidateId;
            return PartialView("AddEditPartial", candidateSkill);

        }

        private IEnumerable<SkillGroup> GetSkillGroups()
        {
            var allskills = _fagService.GetAll().Select(f => f.fag);

            var skillsGrouped = allskills.GroupBy(t => t.ToUpper().Substring(0, 1)).Select(g => new SkillGroup
            {
                Letter = g.Key,
                Skills = g
            });
            return skillsGrouped;
        }

        // POST: CandidateSkill/Create
        [HttpPost]
        public ActionResult Create(CandidateSkill candidateSkill)
        {
            ViewBag.SkillsGrouped = GetSkillGroups(); 

            try
            {
                if (ModelState.IsValid)
                {
                    _candidateSkillService.AddNew(candidateSkill);

                    return RedirectToAction("CandidateSkills", new { candidateId = candidateSkill.CandidateId });
                }
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Ret venligst følgende fejl:");

                return PartialView("AddEditPartial", candidateSkill);
            
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is InvalidOperationException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("",ex.Message);

                return PartialView("AddEditPartial", candidateSkill);
            }
            
        }

        // GET: CandidateSkill/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.SkillsGrouped = GetSkillGroups(); 

            CandidateSkill candidateSkill = _candidateSkillService.GetCandidateSkillsByID(id);

            return PartialView("AddEditPartial", candidateSkill);
        }

        // POST: CandidateSkill/Edit/5
        [HttpPost]
        public ActionResult Edit(CandidateSkill candidateSkill)
        {
            ViewBag.SkillsGrouped = GetSkillGroups(); 

            try
            {
                if (ModelState.IsValid)
                {
                    _candidateSkillService.Update(candidateSkill);

                    return RedirectToAction("CandidateSkills", new { candidateId = candidateSkill.CandidateId });
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("AddEditPartial", candidateSkill);
            }
            catch(Exception ex)
            {
                this.Response.StatusCode = 400;
                if (ex is System.Data.SqlClient.SqlException)
                    ModelState.AddModelError("", "korrigere venligst følgende");
                else
                    ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditPartial", candidateSkill);
            }
        }

        // GET: CandidateSkill/Delete/5
        public ActionResult Delete(int id)
        {
            var candidateSkill = _candidateSkillService.GetCandidateSkillsByID(id);
            _candidateSkillService.Delete(candidateSkill);
            return RedirectToAction("CandidateSkills", new { candidateId = candidateSkill.CandidateId });
        }

        // POST: CandidateSkill/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }

    public class SkillGroup
    {
        public string Letter { get; set; }
        public IEnumerable<string> Skills { get; set; }
    }
}
