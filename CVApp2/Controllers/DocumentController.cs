﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;

namespace CVApp2.Controllers
{
    public class DocumentController : Controller
    {
        private const string PICTURE_PATH = "~/DocumentFiles/";
        IDocumentService _documentService;

        public DocumentController()
        {
            _documentService = new DocumentService();
        }

        // GET: Document



        public ActionResult ShowDocument(int id, string file)
        {
            Document document = _documentService.GetDocumentById(id);

            if (document == null || document.ServerFileName != file)
                return Content("Invalid Request");

            var cd = new System.Net.Mime.ContentDisposition
            {
                // for example foo.bak
                FileName = document.FileName,

                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(Path.Combine("~/DocumentFiles/", document.ServerFileName), MimeMapping.GetMimeMapping(document.FileName));
        }

        public ActionResult Copypath(int id, string file)
        {
            Document document = _documentService.GetDocumentById(id);

            if (file!="")            
            {


                string pcname = Environment.MachineName;


                string fname = document.ServerFileName;
                string localpath = Path.Combine(PICTURE_PATH, fname);

                string filenames = "\\\\" + pcname + "\\CVApp2\\DocumentFiles\\" + fname + "";
                //DirectoryInfo dirInfo = new DirectoryInfo(HostingEnvironment.MapPath("~/DocumentFiles"));

                //string fullPath = Request.MapPath(localpath);
                //string fulllocalPath = dirInfo.FullName + @"\" + fname;

                FileInfo Fname = new FileInfo(filenames);
                if (Fname.Exists)
                {
                    string clientMachineName = (System.Net.Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName);

                    Clip.MyService myservice = new Clip.MyService();
                    string NewFileName = myservice.SendKnowledgePath(filenames, clientMachineName);
                    return Json(new { NewFileName = NewFileName });



                }
                else
                {
                    return Json(new { NewFileName = "" });
                }
            }
            else
            {
                return Json(new { NewFileName = "" });
            }


        }


        [HttpPost]
        public ActionResult Index(string groupIds, string keywords, int pageIndex, int pageSize)
        {
            int pageCount = 1;

            var documentList = _documentService.GetFiltered(groupIds, keywords, pageIndex, pageSize, out pageCount).OrderByDescending(d => d.Created);

            var docs = documentList.Select(x => new
            {
                x.Id,
                Created = x.Created.Value.ToString("dd/MM/yy"),
                x.DocumentGroup,
                x.DocumentGroupId,
                x.FileName,
                x.ServerFileName,
                x.Tags
            });

            return Json(new { pageCount = pageCount, documents = docs });
        }


        // GET: Document/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Document/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Document/Create
        [HttpPost]
        public ActionResult Create(Document document, HttpPostedFileBase FileName)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string documentsFolderPath = @"..\DocumentFiles";
                    string documentsFolderPathOnServer = Server.MapPath(documentsFolderPath);
                    if (!Directory.Exists(documentsFolderPathOnServer))
                    {
                        Directory.CreateDirectory(documentsFolderPathOnServer);
                    }
                    if (FileName.ContentLength > 0)
                    {
                        var guid = Guid.NewGuid();
                        var extention = Path.GetExtension(FileName.FileName);
                        var fileName = guid.ToString() + extention;
                        document.ServerFileName = guid.ToString() + extention;
                        var path = Path.Combine(documentsFolderPathOnServer, fileName);
                        FileName.SaveAs(path);
                        document.Created = System.DateTime.Now;
                        document.FileContent = CandidateFilesController.ReadFileData(path).Replace("'", "''");
                        document.FileName = Path.GetFileName(FileName.FileName);
                        _documentService.AddNew(document);
                        return RedirectToAction("Index", "Knowledge", new { selectedGroup = document.DocumentGroupId });
                    }
                    this.Response.StatusCode = 400;
                    ModelState.AddModelError("", "Invalid or empty file selected, please correct it");
                    return View(document);

                }
                else
                {
                    return View(document);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(document);
            }
        }

        // GET: Document/Edit/5
        public ActionResult Edit(int id)
        {
            Document document = _documentService.GetDocumentById(id);
            return View(document);
        }

        // POST: Document/Edit/5
        [HttpPost]
        public ActionResult Edit(Document document, HttpPostedFileBase FileName)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string documentsFolderPath = @"~\DocumentFiles";
                    string documentsFolderPathOnServer = Server.MapPath(documentsFolderPath);
                    if (!Directory.Exists(documentsFolderPathOnServer))
                    {
                        Directory.CreateDirectory(documentsFolderPathOnServer);
                    }
                    if (FileName != null)
                    {
                        if (System.IO.File.Exists(documentsFolderPathOnServer + "\\" + document.ServerFileName))
                        {
                            System.IO.File.Delete(documentsFolderPathOnServer + "\\" + document.ServerFileName);
                        }
                        if (FileName.ContentLength > 0)
                        {
                            var guid = Guid.NewGuid();
                            var extention = Path.GetExtension(FileName.FileName);
                            var fileName = guid.ToString() + extention;
                            document.ServerFileName = guid.ToString() + extention;
                            var path = Path.Combine(documentsFolderPathOnServer, fileName);
                            FileName.SaveAs(path);
                            document.Created = System.DateTime.Now;
                            document.FileContent = CandidateFilesController.ReadFileData(path).Replace("'", "''");
                            document.FileName = Path.GetFileName(FileName.FileName);
                        }
                    }

                    _documentService.Update(document);
                    return RedirectToAction("Index", "Knowledge", new { selectedGroup = document.DocumentGroupId });


                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return View(document);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "korrigere venligst følgende");

                return View(document);
            }
        }

        // GET: Document/Delete/5
        public ActionResult Delete(int id)
        {

            Document document = _documentService.GetDocumentById(id);
            return PartialView("Delete", document);
        }

        // POST: Document/Delete/5
        [HttpPost]
        public ActionResult Delete(Document document)
        {
            try
            {
                // TODO: Add delete logic here
                string documentsFolderPath = @"~\DocumentFiles";
                string documentsFolderPathOnServer = Server.MapPath(documentsFolderPath);
                _documentService.Delete(document);
                if (System.IO.File.Exists(documentsFolderPathOnServer + "\\" + document.ServerFileName))
                {
                    System.IO.File.Delete(documentsFolderPathOnServer + "\\" + document.ServerFileName);
                }
                return Content("Success");
            }
            catch
            {
                return View(document);
            }
        }

    }
}
