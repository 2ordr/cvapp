﻿using CVApp2.Models;
using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVApp2.Controllers
{
    public class ZipCodeController : Controller
    {
      
          IZipCodeService _ZipCodeService;
          ICityService _CityService;
        public ZipCodeController()
        {
            _ZipCodeService = new ZipCodeService();
            _CityService = new CityService();
            
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: ZipCode/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ZipCode/Create
        public ActionResult Create(int cityId)
        {
            ZipCode zipcode = new ZipCode { CityId = cityId };

            return PartialView("AddEditZipCodePartial", zipcode);
        }

        // POST: ZipCode/Create
        [HttpPost]
        public ActionResult Create(ZipCode zipcode)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ZipCodeService.AddNew(zipcode);

                    return Content("Success");  // RedirectToAction("Details", "City", new { id = Session["CityId"] });
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Ret venligst følgende fejl:");

                return PartialView("AddEditZipCodePartial", zipcode);


            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                
                ModelState.AddModelError("", ex.Message);

                return PartialView("AddEditZipCodePartial", zipcode);
            }
        }

        // GET: ZipCode/Edit/5
        public ActionResult Edit(int id)
        {
            ZipCode zipcode = _ZipCodeService.GetZipCodesByID(id);

            return PartialView("AddEditZipCodePartial", zipcode);
        }

        // POST: ZipCode/Edit/5
        [HttpPost]
        public ActionResult Edit(ZipCode zipcode)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _ZipCodeService.Update(zipcode);

                    return Content("Success");
                }

                this.Response.StatusCode = 400;
                ModelState.AddModelError("", "Please correct following errors:");

                return PartialView("AddEditZipCodePartial", zipcode);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView("AddEditZipCodePartial", zipcode);
            }
        }

        // GET: ZipCode/Delete/5
        public ActionResult Delete(int id)
        {
            var zipcode = _ZipCodeService.GetZipCodesByID(id);

            return PartialView(zipcode);
            
        }

        // POST: ZipCode/Delete/5
        [HttpPost]
        public ActionResult Delete(ZipCode zipcode, FormCollection collection)
        {
            try
            {
                _ZipCodeService.Delete(zipcode);
                return Content("Deleted");
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = 400;
                ModelState.AddModelError("", ex.Message);
                return PartialView(zipcode);
            }
        }
    }
}
