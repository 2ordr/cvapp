﻿using CVApp2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVApp2.Controllers
{
    public class ReportsController : Controller
    {

        ICandidateService _candidateService;

        public ReportsController()
        {
            _candidateService = new CandidateService();

        }
        
        public ActionResult LatestCreated()
        {
            return View();

        }


        [HttpPost]
        public ActionResult LatestCreated(int dateType, DateTime from, DateTime to)
        {
            Tuple<DateTime,DateTime> dateRange = Tuple.Create(from,to);

            var candidates = _candidateService.GetCandidates((DateType) dateType,   dateRange);
            return Json(candidates);
        }
    }
} 