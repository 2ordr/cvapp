﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.ViewModels
{
    public class CandidateTagVM : CandidateTag
    {
        public bool IsSelected { get; set; }
    }
}