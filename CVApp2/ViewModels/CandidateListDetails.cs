﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.ViewModels
{
    public class CandidateListDetails : Candidate
    {
        public string SkillsCSV { get; set; }

        public string City { get; set; }

        public string Zipcode { get; set; }

        public string Files { get; set; }

        public string CandidateType { get; set; }
    }
}