﻿using CVApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.ViewModels
{
    public class SearchVM
    {
        public  IList<Region> Regions { get; set; }
        public IList<Candidate> Candidates { get; set; }

        public IList<ProfessionTag> Tags { get; set; }

        public IList<string> Skills { get; set; }

        public string CVUrl { get; set; }
    }
}