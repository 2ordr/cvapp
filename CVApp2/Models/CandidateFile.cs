﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class CandidateFile : BaseEntity
    {
        public int CandidateId { get; set; }
        public string FileContent { get; set; }
        public string FileName { get; set; }
    }
}