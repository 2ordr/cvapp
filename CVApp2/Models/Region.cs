﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class Region : BaseEntity
    {
        
        [Required]
        [MaxLength(50)]
        public string RegionName { get; set; }

        public bool IsAutoSelected { get; set; }
    }
}