﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class ZipCode : BaseEntity
    {
          [Required]
        public int CityId { get; set; }

        public string ZipCodeNumber { get; set; }

        public virtual City City { get; set; }

        
    }
}