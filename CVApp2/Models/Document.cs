﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CVApp2.Models
{
    public class Document : BaseEntity
    {
        [MaxLength(255)]
        public string FileName { get; set; }  //just stores users filename, physically file will be stored with different name for security reasons

        [Required(ErrorMessage = "Vælg dokument gruppe")]
        [Range(1, int.MaxValue, ErrorMessage = "Vælg dokument gruppe")]
        public int DocumentGroupId { get; set; }



        public string JNo { get; set; }

        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? JDate { get; set; }


        public string ReleasedForPublication { get; set; } //either date or comment



        public string KarnovNo { get; set; }

        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? KarnovDate { get; set; }


        public string TBBNo { get; set; }

        [DisplayFormat(NullDisplayText="",DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? TBBDate { get; set; }


        public string ArbitrationCourtComposition { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string Summary { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string Tags { get; set; }


        public string Comments { get; set; }


        
        // our internal purpose fields

        [Column(TypeName = "varchar(MAX)")]
        [AllowHtml]
        public string FileContent { get; set; } //for pdf and doc files, text will be extracted and saved for quick search

        [MaxLength(255)]
        public string ServerFileName { get; set; }


        public DateTime? Created { get; set; }

        




       

        public virtual DocumentGroup DocumentGroup { get; set; }
    }
}

