﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class City : BaseEntity
    {
        [Required,MaxLength(50)]
        public string CityName { get; set; }

        [Required]
        public int RegionId { get; set; }

        public virtual Region Region { get; set; }

       

        public string ZipCode { get; set; }
    }
}