﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class CandidateSkill : BaseEntity
    {
        public int CandidateId { get; set; }

        [Required,MaxLength(250)]
        public string Skill { get; set; }
    }
}