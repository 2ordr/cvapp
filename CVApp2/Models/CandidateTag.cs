﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class CandidateTag : BaseEntity
    {
        public int CandidateId { get; set; }

        public int TagId { get; set; }

        public virtual ProfessionTag ProfessionTag { get; set; }

        public virtual Candidate Candidate { get; set; }
    }
}