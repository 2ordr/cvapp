﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class CandidateNotes : BaseEntity
    {
        public int CandidateId { get; set; }

        [Required(ErrorMessage = "Nødvendig")]
        public string Note { get; set; }

        [DisplayFormat(DataFormatString="{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        public string UserName { get; set; }
    }
}