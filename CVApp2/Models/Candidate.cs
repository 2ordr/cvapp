﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{

    public enum CandidateStatus
    {
        Standby = -1,
        Passive = 0,
        Active = 1
    }




    public class Candidate : BaseEntity
    {
        
        [Required(ErrorMessage="Nødvendig") ,MaxLength(50)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nødvendig"), MaxLength(50)]
        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string RegistrationId { get; set; }

        [DataType(DataType.PhoneNumber)]
      
        public string PhoneHome { get; set; }

        [DataType(DataType.PhoneNumber)]
     
        public string PhoneWork { get; set; }


        [DataType(DataType.PhoneNumber)]
   
        public string PhoneMobile { get; set; }

        public string Fax { get; set; }

        public string Address { get; set; }

        [Required]
        public int ZipcodeId { get; set; }

        public string Notes { get; set; }

        public string Company { get; set; }

        public string Designation { get; set; }

        [DataType(DataType.Url)]
        public string Website { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public CandidateStatus Status { get; set; }

        public DateTime LastUpdated { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastAccessed { get; set; }


        public string State { get; set; }

        // virtual properties

        public virtual IList<string> Skills { get; set; }

        public virtual string City { get; set; }

        [Required(ErrorMessage = "Nødvendig"), MaxLength(20)]
        public virtual string Zipcode { get; set; }

        public virtual string Region { get; set; }

        [Range(0,5)]
        public int Ratings { get; set; }

        public virtual List<string> ProfessionTags { get; set; }

        public virtual List<string> CandidateFiles { get; set; }

        public virtual List<PreferredRegion> PreferredRegions { get; set; }

        public bool Bygherre { get; set; }

        public bool Rådgiver { get; set; }

        public bool Entreprenør { get; set; }

        public string CUrl { get; set; }

       

    }
}