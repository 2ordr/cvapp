﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class PreferredRegion : BaseEntity
    {
        public int CandidateId { get; set; }
        public int RegionId { get; set; }

        public virtual Region Region { get; set; }
    }
}