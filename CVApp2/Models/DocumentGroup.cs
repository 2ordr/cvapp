﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class DocumentGroup : BaseEntity
    {
        [Required]
        [StringLength(50,ErrorMessage="Name cannot be longer than 50 characters")]
        public string Name { get; set; }

       [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }

        public int ParentGroupId { get; set; }

    }
}