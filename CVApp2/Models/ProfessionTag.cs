﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVApp2.Models
{
    public class ProfessionTag : BaseEntity
    {
        [Required]
        public string Tag { get; set; }
    }
}